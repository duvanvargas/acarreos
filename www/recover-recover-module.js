(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["recover-recover-module"],{

/***/ "./src/app/recover/recover.module.ts":
/*!*******************************************!*\
  !*** ./src/app/recover/recover.module.ts ***!
  \*******************************************/
/*! exports provided: RecoverPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverPageModule", function() { return RecoverPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _recover_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recover.page */ "./src/app/recover/recover.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _recover_page__WEBPACK_IMPORTED_MODULE_5__["RecoverPage"]
    }
];
var RecoverPageModule = /** @class */ (function () {
    function RecoverPageModule() {
    }
    RecoverPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_recover_page__WEBPACK_IMPORTED_MODULE_5__["RecoverPage"]]
        })
    ], RecoverPageModule);
    return RecoverPageModule;
}());



/***/ }),

/***/ "./src/app/recover/recover.page.html":
/*!*******************************************!*\
  !*** ./src/app/recover/recover.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding text-center>\n  <div class=\"form\">\n    <img src=\"assets/img/logoicon.png\" class=\"login-image\" />\n    <h1>Recupera tu contraseña</h1>\n    <p>Por favor ingresa con tu correo</p>\n    <br />\n    <ion-list margin-top margin-bottom>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"floating\">Email</ion-label>\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          tabindex=\"20\"\n          (keyup.enter)=\"moveFocus(b)\"\n          placeholder=\"Email\"\n          [(ngModel)]=\"username\"\n          required\n        ></ion-input>\n      </ion-item>\n    </ion-list>\n    <br />\n    <ion-button\n      class=\"goodfont boton\"\n      margin-bottom\n      mode=\"ios\"\n      expand=\"block\"\n      [disabled]=\"username.trim().length===0\"\n      (click)=\"recover()\"\n      >Recuperar</ion-button\n    >\n    <p class=\"register-text\" (click)=\"goLogin()\">\n      ¿Ya tienes una cuenta? <strong><a> Ingresa</a></strong>\n    </p>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/recover/recover.page.scss":
/*!*******************************************!*\
  !*** ./src/app/recover/recover.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlY292ZXIvcmVjb3Zlci5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/recover/recover.page.ts":
/*!*****************************************!*\
  !*** ./src/app/recover/recover.page.ts ***!
  \*****************************************/
/*! exports provided: RecoverPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverPage", function() { return RecoverPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RecoverPage = /** @class */ (function () {
    function RecoverPage(afs, rout, alertController) {
        this.afs = afs;
        this.rout = rout;
        this.alertController = alertController;
        this.username = "";
    }
    RecoverPage.prototype.recover = function () {
        this.afs.auth
            .sendPasswordResetEmail(this.username)
            .then(function () {
            alert("Te hemos enviado un correo para que puedas reestablecer tu contraseña");
        })
            .catch(function (error) {
            console.log(error);
            alert("Ocurrio un error");
        });
    };
    RecoverPage.prototype.goLogin = function () {
        this.rout.navigateByUrl("/login");
    };
    RecoverPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-recover",
            template: __webpack_require__(/*! ./recover.page.html */ "./src/app/recover/recover.page.html"),
            styles: [__webpack_require__(/*! ./recover.page.scss */ "./src/app/recover/recover.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
    ], RecoverPage);
    return RecoverPage;
}());



/***/ })

}]);
//# sourceMappingURL=recover-recover-module.js.map