(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["service-detail-service-detail-module"],{

/***/ "./src/app/safe.pipe.ts":
/*!******************************!*\
  !*** ./src/app/safe.pipe.ts ***!
  \******************************/
/*! exports provided: SafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return SafePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: "safe" }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"]])
    ], SafePipe);
    return SafePipe;
}());



/***/ }),

/***/ "./src/app/service-detail/service-detail.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/service-detail/service-detail.module.ts ***!
  \*********************************************************/
/*! exports provided: ServiceDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceDetailPageModule", function() { return ServiceDetailPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _safe_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../safe.pipe */ "./src/app/safe.pipe.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _service_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service-detail.page */ "./src/app/service-detail/service-detail.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: "",
        component: _service_detail_page__WEBPACK_IMPORTED_MODULE_6__["ServiceDetailPage"],
    },
];
var ServiceDetailPageModule = /** @class */ (function () {
    function ServiceDetailPageModule() {
    }
    ServiceDetailPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            providers: [_safe_pipe__WEBPACK_IMPORTED_MODULE_4__["SafePipe"]],
            declarations: [_service_detail_page__WEBPACK_IMPORTED_MODULE_6__["ServiceDetailPage"], _safe_pipe__WEBPACK_IMPORTED_MODULE_4__["SafePipe"]],
        })
    ], ServiceDetailPageModule);
    return ServiceDetailPageModule;
}());



/***/ }),

/***/ "./src/app/service-detail/service-detail.page.html":
/*!*********************************************************!*\
  !*** ./src/app/service-detail/service-detail.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title text-center>Servicio ({{myServiceId}})</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content scroll=\"false\">\n  <div padding *ngIf=\"serviceInfo\">\n    <iframe\n      *ngIf=\"serviceInfo.dirRecogida && serviceInfo.dirDestino\"\n      [src]=\"'https://www.google.com/maps/embed/v1/directions?key=AIzaSyB1KLpJ1-CMeux7HEZZnO_CNdtfr7o0cWI&origin='+encodeURL(serviceInfo.dirRecogida +' '+serviceInfo.dirRecogidaExtra)+ ', Colombia&destination='+ encodeURL(serviceInfo.dirDestino + ' ' + serviceInfo.dirDestinoExtra) + ', Colombia&avoid=tolls|highways&zoom=12'  | safe\"\n      title=\"W3Schools Free Online Web Tutorials\"\n    >\n    </iframe>\n    <p>\n      <ion-icon name=\"arrow-up\"></ion-icon>\n    </p>\n    <p># {{myServiceId}}</p>\n    <p>\n      <ion-icon name=\"pin\"></ion-icon>\n    </p>\n    <div class=\"address-container\">\n      <div>\n        <p>Origen</p>\n        <p><strong>{{ serviceInfo.dirRecogida }}</strong></p>\n      </div>\n      <div>\n        <p>Destino:</p>\n        <p><strong>{{ serviceInfo.dirDestino }}</strong></p>\n      </div>\n    </div>\n\n    <p>Estado:</p>\n    <p>{{ serviceInfo.status }}</p>\n    <div class=\"driver-card\" *ngIf=\"serviceInfo.status === 'PENDING'\">\n      <img src=\"assets/img/acarreos.PNG\" alt=\"\" />\n      <div class=\"detail-user\">\n        <strong>Esperando conductor...</strong>\n      </div>\n      <div class=\"blue\"></div>\n    </div>\n    <div class=\"driver-card\" *ngIf=\"serviceInfo.status === 'TAKEN'\">\n      <img src=\"assets/img/acarreos.PNG\" alt=\"\" />\n      <div class=\"detail-user\">\n        <strong>Miller Guayabo</strong>\n        <p>5 trayectos</p>\n      </div>\n      <div class=\"blue\">Acarreos</div>\n    </div>\n    <form class=\"form-container\" novalidate=\"\" autocomplete=\"off\">\n      <label for=\"\">\n        Fecha recogida:\n        <input\n          type=\"phone\"\n          readonly\n          class=\"readonly\"\n          placeholder=\"{{ serviceInfo.fechaRecogida }}\"\n        />\n      </label>\n      <br />\n      <label for=\"\">\n        Dimensiones:\n        <input\n          type=\"phone\"\n          readonly\n          class=\"readonly\"\n          placeholder=\"{{ serviceInfo.dimensiones.alto }}x{{ serviceInfo.dimensiones.ancho }}x{{ serviceInfo.dimensiones.largo }}\"\n        />\n      </label>\n      <label for=\"\">\n        Anotaciones:\n        <input\n          type=\"phone\"\n          readonly\n          class=\"readonly\"\n          placeholder=\"{{ serviceInfo.anotaciones }}\"\n        />\n      </label>\n      <br />\n    </form>\n    <br />\n    <br />\n    <br />\n    <br />\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/service-detail/service-detail.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/service-detail/service-detail.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p {\n  margin-top: 0px;\n  margin-bottom: 5px;\n  color: rgba(128, 128, 128, 0.8); }\n\n.item-profile {\n  width: 100%;\n  background-position: center center;\n  background-size: cover; }\n\n.item-detail {\n  width: 100%;\n  background: white;\n  position: absolute; }\n\n.payment {\n  height: 50px;\n  display: flex;\n  margin: auto;\n  margin-top: 20px;\n  margin-bottom: 20px; }\n\n.driver-card {\n  border-top: 1px solid rgba(128, 128, 128, 0.3);\n  border-bottom: 1px solid rgba(128, 128, 128, 0.3);\n  display: flex;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  align-items: center;\n  justify-content: space-between; }\n\n.driver-card img {\n    height: 50px; }\n\n.driver-card .detail-user {\n    flex: 2;\n    padding-left: 10px; }\n\n.driver-card .detail-user p {\n      text-align: left;\n      margin: 0; }\n\n.driver-card .blue {\n    color: #4c6ade;\n    font-weight: bold; }\n\n.address-container {\n  display: flex;\n  justify-content: space-around; }\n\n.address-container div p strong {\n    color: black; }\n\niframe {\n  width: 100%;\n  height: 200px;\n  border: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VydmljZS1kZXRhaWwvQzpcXFVzZXJzXFxBbGV4YW5kZXJWYXJnYXNcXERvY3VtZW50c1xcZGV2ZWxvcG1lbnRzeXNcXGFjYXJyZW9zL3NyY1xcYXBwXFxzZXJ2aWNlLWRldGFpbFxcc2VydmljZS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWU7RUFDZixtQkFBa0I7RUFDbEIsZ0NBQStCLEVBQ2hDOztBQUNEO0VBQ0UsWUFBVztFQUNYLG1DQUFrQztFQUNsQyx1QkFBc0IsRUFDdkI7O0FBRUQ7RUFDRSxZQUFXO0VBQ1gsa0JBQWlCO0VBQ2pCLG1CQUFrQixFQUNuQjs7QUFDRDtFQUNFLGFBQVk7RUFDWixjQUFhO0VBQ2IsYUFBWTtFQUNaLGlCQUFnQjtFQUNoQixvQkFBbUIsRUFDcEI7O0FBQ0Q7RUFDRSwrQ0FBOEM7RUFDOUMsa0RBQWlEO0VBQ2pELGNBQWE7RUFDYixrQkFBaUI7RUFDakIscUJBQW9CO0VBQ3BCLG9CQUFtQjtFQUNuQiwrQkFBOEIsRUFnQi9COztBQXZCRDtJQVNJLGFBQVksRUFDYjs7QUFWSDtJQVlJLFFBQU87SUFDUCxtQkFBa0IsRUFLbkI7O0FBbEJIO01BZU0saUJBQWdCO01BQ2hCLFVBQVMsRUFDVjs7QUFqQkw7SUFvQkksZUFBYztJQUNkLGtCQUFpQixFQUNsQjs7QUFHSDtFQUNFLGNBQWE7RUFDYiw4QkFBNkIsRUFROUI7O0FBVkQ7SUFNUSxhQUFZLEVBQ2I7O0FBS1A7RUFDRSxZQUFXO0VBQ1gsY0FBYTtFQUNiLGFBQVksRUFDYiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2UtZGV0YWlsL3NlcnZpY2UtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInAge1xyXG4gIG1hcmdpbi10b3A6IDBweDtcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgY29sb3I6IHJnYmEoMTI4LCAxMjgsIDEyOCwgMC44KTtcclxufVxyXG4uaXRlbS1wcm9maWxlIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuXHJcbi5pdGVtLWRldGFpbCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5wYXltZW50IHtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5kcml2ZXItY2FyZCB7XHJcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMTI4LCAxMjgsIDEyOCwgMC4zKTtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgxMjgsIDEyOCwgMTI4LCAwLjMpO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgaW1nIHtcclxuICAgIGhlaWdodDogNTBweDtcclxuICB9XHJcbiAgLmRldGFpbC11c2VyIHtcclxuICAgIGZsZXg6IDI7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICBwIHtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICAuYmx1ZSB7XHJcbiAgICBjb2xvcjogIzRjNmFkZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxufVxyXG5cclxuLmFkZHJlc3MtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gIGRpdiB7XHJcbiAgICBwIHtcclxuICAgICAgc3Ryb25nIHtcclxuICAgICAgICBjb2xvcjogYmxhY2s7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmlmcmFtZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/service-detail/service-detail.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/service-detail/service-detail.page.ts ***!
  \*******************************************************/
/*! exports provided: ServiceDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceDetailPage", function() { return ServiceDetailPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiceDetailPage = /** @class */ (function () {
    function ServiceDetailPage(activatedRoute, servicesService) {
        this.activatedRoute = activatedRoute;
        this.servicesService = servicesService;
        this.myServiceId = null;
        this.serviceInfo = null;
    }
    ServiceDetailPage.prototype.encodeURL = function (url) {
        return encodeURIComponent(url);
    };
    ServiceDetailPage.prototype.ngOnInit = function () {
        var _this = this;
        this.myServiceId = this.activatedRoute.snapshot.paramMap.get("myServiceId");
        this.servicesService
            .acarreoDetail(this.myServiceId)
            .then(function (doc) {
            console.log(doc);
            _this.serviceInfo = doc;
        })
            .catch(function (err) {
            alert("Este viaje no existe");
        });
    };
    ServiceDetailPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-service-detail",
            template: __webpack_require__(/*! ./service-detail.page.html */ "./src/app/service-detail/service-detail.page.html"),
            styles: [__webpack_require__(/*! ./service-detail.page.scss */ "./src/app/service-detail/service-detail.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_services_service__WEBPACK_IMPORTED_MODULE_2__["ServicesService"]])
    ], ServiceDetailPage);
    return ServiceDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=service-detail-service-detail-module.js.map