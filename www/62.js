(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[62],{

/***/ "./src/app/formularioGrua/formularioGrua.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/formularioGrua/formularioGrua.module.ts ***!
  \*********************************************************/
/*! exports provided: FormularioGruaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioGruaPageModule", function() { return FormularioGruaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _formularioGrua_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./formularioGrua.page */ "./src/app/formularioGrua/formularioGrua.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: "",
        component: _formularioGrua_page__WEBPACK_IMPORTED_MODULE_5__["FormularioGruaPage"],
    },
];
var FormularioGruaPageModule = /** @class */ (function () {
    function FormularioGruaPageModule() {
    }
    FormularioGruaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_formularioGrua_page__WEBPACK_IMPORTED_MODULE_5__["FormularioGruaPage"]],
        })
    ], FormularioGruaPageModule);
    return FormularioGruaPageModule;
}());



/***/ }),

/***/ "./src/app/formularioGrua/formularioGrua.page.html":
/*!*********************************************************!*\
  !*** ./src/app/formularioGrua/formularioGrua.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button [routerLink]=\"['/tabs/service']\">\n        <ion-icon\n          mode=\"ios\"\n          color=\"primary\"\n          slot=\"icon-only\"\n          name=\"arrow-back\"\n        ></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title text-center>Formulario Grúa</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content scroll=\"false\">\n  <div padding>\n    <img src=\"assets/img/logoicon.png\" class=\"login-image\" />\n    <h1>Grúa</h1>\n    <p><strong>Por favor ingrese los datos para su servicio</strong></p>\n\n    <ion-list margin-top margin-bottom>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Fecha recogida</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"date\"\n          placeholder=\"Fecha recogida\"\n          [(ngModel)]=\"formAcarreo.fechaRecogida\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Dirección Origen</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Dirección Origen\"\n          [(ngModel)]=\"formAcarreo.dirRecogida\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >(piso, apto , bodega )</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Dirección Origen\"\n          [(ngModel)]=\"formAcarreo.dirRecogidaExtra\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Placa vehículo</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"ABC123\"\n          [(ngModel)]=\"formAcarreo.placa_vehiculo\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\">Anotaciones</ion-label>\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Anotaciones\"\n          [(ngModel)]=\"formAcarreo.anotaciones\"\n          required\n        ></ion-input>\n      </ion-item>\n    </ion-list>\n    <ion-button\n      [disabled]=\"!checkForm()\"\n      class=\"goodfont boton\"\n      margin-bottom\n      mode=\"ios\"\n      expand=\"block\"\n      (click)=\"crearViaje()\"\n      >SOLICITAR</ion-button\n    >\n    <br />\n    <br />\n    <br />\n    <br />\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/formularioGrua/formularioGrua.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/formularioGrua/formularioGrua.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".payment {\n  display: flex;\n  margin: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybXVsYXJpb0dydWEvQzpcXFVzZXJzXFxBbGV4YW5kZXJWYXJnYXNcXERvY3VtZW50c1xcZGV2ZWxvcG1lbnRzeXNcXGFjYXJyZW9zL3NyY1xcYXBwXFxmb3JtdWxhcmlvR3J1YVxcZm9ybXVsYXJpb0dydWEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYTtFQUNiLGFBQVksRUFDYiIsImZpbGUiOiJzcmMvYXBwL2Zvcm11bGFyaW9HcnVhL2Zvcm11bGFyaW9HcnVhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYXltZW50IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIG1hcmdpbjogYXV0bztcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/formularioGrua/formularioGrua.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/formularioGrua/formularioGrua.page.ts ***!
  \*******************************************************/
/*! exports provided: FormularioGruaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioGruaPage", function() { return FormularioGruaPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var FormularioGruaPage = /** @class */ (function () {
    function FormularioGruaPage(servicesService, aut, loadingController) {
        this.servicesService = servicesService;
        this.aut = aut;
        this.loadingController = loadingController;
        /**
         * FECHA RECOGIDA
              DIMENSIONES
              UBICACIÓN
              DISTANCIA ENTRE CAMION Y LA PUERTA
              DIRECCION RECOGIDA
              DIRECCIÓN DESTINO
              CONDICIONES SERVICIO
         */
        this.formAcarreo = {
            tipo: "Grua",
            date: Date.now(),
            status: "PENDING",
            userId: null,
            driverId: 0,
            fechaRecogida: null,
            dimensiones: {
                alto: null,
                ancho: null,
                largo: null,
            },
            dirRecogida: "",
            dirRecogidaExtra: "",
            dirDestino: "",
            dirDestinoExtra: "",
            placa_vehiculo: "",
            anotaciones: "",
            distanciaCamion: "",
        };
    }
    FormularioGruaPage.prototype.ngOnInit = function () {
        var _this = this;
        this.aut.authState.subscribe(function (user) {
            if (user) {
                _this.userLogged = user;
            }
        });
    };
    FormularioGruaPage.prototype.crearViaje = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.formAcarreo.userId = this.userLogged.uid;
                        console.log(this.formAcarreo);
                        return [4 /*yield*/, this.loadingController.create({
                                message: "Creando viaje...",
                                duration: 2000,
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.servicesService.createAcarreo(this.formAcarreo);
                        return [2 /*return*/];
                }
            });
        });
    };
    FormularioGruaPage.prototype.checkForm = function () {
        var form = this.formAcarreo;
        return (this.isValid(form.fechaRecogida) &&
            this.isValid(form.dirRecogida) &&
            this.isValid(form.placa_vehiculo));
    };
    FormularioGruaPage.prototype.isValid = function (field) {
        if (field === "")
            return false;
        if (field === null)
            return false;
        return true;
    };
    FormularioGruaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-formulario",
            template: __webpack_require__(/*! ./formularioGrua.page.html */ "./src/app/formularioGrua/formularioGrua.page.html"),
            styles: [__webpack_require__(/*! ./formularioGrua.page.scss */ "./src/app/formularioGrua/formularioGrua.page.scss")]
        }),
        __metadata("design:paramtypes", [_services_services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], FormularioGruaPage);
    return FormularioGruaPage;
}());



/***/ })

}]);
//# sourceMappingURL=62.js.map