(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-profile-edit-profile-module"],{

/***/ "./src/app/edit-profile/edit-profile.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.module.ts ***!
  \*****************************************************/
/*! exports provided: EditProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function() { return EditProfilePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/edit-profile/edit-profile.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_5__["EditProfilePage"]
    }
];
var EditProfilePageModule = /** @class */ (function () {
    function EditProfilePageModule() {
    }
    EditProfilePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_5__["EditProfilePage"]]
        })
    ], EditProfilePageModule);
    return EditProfilePageModule;
}());



/***/ }),

/***/ "./src/app/edit-profile/edit-profile.page.html":
/*!*****************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\r\n  <ion-toolbar no-border>\r\n    <ion-title text-center>Cuenta usuario</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <div class=\"form\">\r\n    <div text-center>\r\n      <img\r\n        *ngIf=\"img !== 'null' && img && !urlImage\"\r\n        src=\"{{img}}\"\r\n        class=\"circle\"\r\n      />\r\n      <img\r\n        *ngIf=\"img !== 'null' && urlImage\"\r\n        [src]=\"urlImage | async\"\r\n        class=\"circle\"\r\n      />\r\n\r\n      <br />\r\n      <br />\r\n      <img\r\n        *ngIf=\"img === 'null' || !urlImage && !img\"\r\n        src=\"assets/user.svg\"\r\n        width=\"200px\"\r\n      />\r\n      <br />\r\n    </div>\r\n\r\n    <div margin-bottom margin-top text-center>\r\n      <label class=\"custom-file-upload\" style=\"margin: auto; max-width: 400px\">\r\n        <input\r\n          id=\"file-upload\"\r\n          placeholder=\"Seleciona imagen\"\r\n          type=\"file\"\r\n          accept=\".png, .jpg\"\r\n          (change)=\"onUpload($event)\"\r\n        />\r\n        <ion-icon margin-right name=\"cloud-download\"></ion-icon> Cargar foto\r\n      </label>\r\n    </div>\r\n\r\n    <br />\r\n    <h3 class=\"goodfont\">{{name}}</h3>\r\n\r\n    <br />\r\n    <ion-item no-lines margin-bottom margin-bottom>\r\n      <ion-label position=\"floating\">Nombre</ion-label>\r\n      <ion-input\r\n        class=\"inputForm\"\r\n        (keyup.enter)=\"moveFocus(c)\"\r\n        type=\"text\"\r\n        #b\r\n        placeholder=\"Name\"\r\n        [(ngModel)]=\"name\"\r\n      ></ion-input>\r\n    </ion-item>\r\n    <ion-item no-lines margin-bottom margin-bottom>\r\n      <ion-label position=\"floating\">Telefono</ion-label>\r\n      <ion-input\r\n        class=\"inputForm\"\r\n        #c\r\n        (keyup.enter)=\"moveFocus(d)\"\r\n        type=\"text\"\r\n        placeholder=\"Phone\"\r\n        [(ngModel)]=\"phone\"\r\n      ></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item no-lines margin-bottom margin-bottom>\r\n      <ion-label position=\"floating\">Email</ion-label>\r\n      <ion-input class=\"inputForm\" [(ngModel)]=\"mail\" disabled></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item no-lines margin-bottom margin-bottom>\r\n      <ion-label position=\"floating\">Dirección</ion-label>\r\n      <ion-input\r\n        class=\"inputForm\"\r\n        #d\r\n        (keyup.enter)=\"save(name, phone,adress)\"\r\n        type=\"text\"\r\n        placeholder=\"Direcction\"\r\n        [(ngModel)]=\"adress\"\r\n      >\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <input\r\n      #imageProd\r\n      type=\"hidden\"\r\n      [value]=\"urlImage | async\"\r\n      style=\"color: black\"\r\n    />\r\n    <br /><br />\r\n    <ion-button\r\n      class=\"goodfont boton\"\r\n      margin-bottom\r\n      mode=\"ios\"\r\n      expand=\"block\"\r\n      (click)=\"save(name, phone,adress, username)\"\r\n      >Guardar</ion-button\r\n    >\r\n  </div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/edit-profile/edit-profile.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\nh4 {\n  color: #222428;\n  font-size: 17px;\n  font-weight: bold; }\n\n#file-upload {\n  font-size: 14px; }\n\n.titulo {\n  color: #fcd000;\n  font-size: 24px;\n  font-weight: bold; }\n\nh1 {\n  font-size: 40px;\n  font-weight: bold;\n  text-align: center; }\n\n.boton ion-button {\n  height: 39px;\n  border-radius: 50px;\n  font-size: 17px;\n  font-weight: bold; }\n\n.sep {\n  margin-top: 10px; }\n\nh2 {\n  font-size: 1.5rem;\n  color: #232b38; }\n\n.input-card {\n  border-radius: 5rem;\n  background: #f5f6f7;\n  box-shadow: 0 3px 80px rgba(39, 68, 74, 0.2); }\n\nform.input-box {\n  border: 2px solid #c5cccd;\n  border-radius: 1rem;\n  background: #ffffff;\n  transition: 0.2s all; }\n\nform.input-box:focus-within {\n  border: 2px solid #02c4d9; }\n\nform.input-box:focus-within.error {\n  border: 2px solid #f54d3d; }\n\ninput {\n  border: none;\n  background: transparent;\n  padding: 1.125rem 1rem;\n  width: 95%;\n  font-family: \"Poppins\", sans-serif;\n  font-weight: 500;\n  font-size: 1.5rem;\n  transition: 0.2s all; }\n\ninput:not(:last-child) {\n  border-bottom: 2px solid #eceeee; }\n\ninput::-moz-placeholder {\n  color: #9da8ab; }\n\ninput:-ms-input-placeholder {\n  color: #9da8ab; }\n\ninput::placeholder {\n  color: #9da8ab; }\n\ninput:focus {\n  outline: none;\n  color: #08242a;\n  padding: 2rem 1rem; }\n\ninput:focus::-moz-placeholder {\n  color: #758589; }\n\ninput:focus:-ms-input-placeholder {\n  color: #758589; }\n\ninput:focus::placeholder {\n  color: #758589; }\n\ninput.error {\n  color: #f54d3d; }\n\ninput.success {\n  color: #02c4d9; }\n\n.buttone {\n  position: relative;\n  border: none;\n  padding: 1rem 3rem;\n  margin: 1rem;\n  border-radius: 99999px;\n  font-size: 1.5rem;\n  font-weight: 700;\n  font-family: \"Poppins\", sans-serif;\n  transition: 0.2s all;\n  transition-timing-function: ease; }\n\n.buttone:hover {\n  transform: translatey(3px); }\n\n.buttone:focus {\n  outline: none; }\n\n.buttone.teal {\n  background-color: #02c4d9;\n  box-shadow: 0 7px 50px rgba(2, 196, 217, 0.5);\n  color: #ffffff; }\n\n.buttone.teal:hover {\n  box-shadow: 0 2px 10px rgba(2, 196, 217, 0.5); }\n\n.buttone.google {\n  background-color: #ffffff;\n  box-shadow: 0 3px 20px rgba(39, 68, 74, 0.2);\n  color: #506569;\n  font-weight: 600;\n  font-size: 22px;\n  line-height: 1rem; }\n\n.buttone.google > img {\n  height: 20px;\n  width: 20px; }\n\n.buttone.google:hover {\n  box-shadow: 0 1px 5px rgba(39, 68, 74, 0.2); }\n\n.ionicon {\n  font-size: 90px; }\n\n.fileUpload {\n  position: relative;\n  overflow: hidden;\n  margin: 10px; }\n\n.fileUpload input.upload {\n  position: absolute;\n  top: 0;\n  right: 0;\n  margin: 0;\n  padding: 0;\n  font-size: 20px;\n  cursor: pointer;\n  opacity: 0;\n  filter: alpha(opacity=0); }\n\ninput[type=\"file\"] {\n  display: none; }\n\n.custom-file-upload {\n  border: 1px solid #ccc;\n  display: inline-block;\n  padding: 6px 12px;\n  cursor: pointer;\n  width: 100%;\n  text-align: center;\n  color: var(--ion-color-primary); }\n\n.goodfont {\n  text-align: center; }\n\n.circle {\n  border-radius: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWRpdC1wcm9maWxlL0M6XFxVc2Vyc1xcQWxleGFuZGVyVmFyZ2FzXFxEb2N1bWVudHNcXGRldmVsb3BtZW50c3lzXFxhY2FycmVvcy9zcmNcXGFwcFxcZWRpdC1wcm9maWxlXFxlZGl0LXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWdCO0VBQ2hCLGlCQUFnQixFQUNqQjs7QUFDRDtFQUNFLGVBQWM7RUFDZCxnQkFBZTtFQUNmLGtCQUFpQixFQUNsQjs7QUFFRDtFQUNFLGdCQUFlLEVBQ2hCOztBQUVEO0VBQ0UsZUFBYztFQUNkLGdCQUFlO0VBQ2Ysa0JBQWlCLEVBQ2xCOztBQUVEO0VBQ0UsZ0JBQWU7RUFDZixrQkFBaUI7RUFDakIsbUJBQWtCLEVBQ25COztBQUVEO0VBQ0UsYUFBWTtFQUNaLG9CQUFtQjtFQUNuQixnQkFBZTtFQUNmLGtCQUFpQixFQUNsQjs7QUFFRDtFQUNFLGlCQUFnQixFQUNqQjs7QUFDRDtFQUNFLGtCQUFpQjtFQUNqQixlQUFjLEVBQ2Y7O0FBRUQ7RUFDRSxvQkFBbUI7RUFDbkIsb0JBQW1CO0VBQ25CLDZDQUE0QyxFQUM3Qzs7QUFFRDtFQUNFLDBCQUF5QjtFQUN6QixvQkFBbUI7RUFDbkIsb0JBQW1CO0VBQ25CLHFCQUFvQixFQUNyQjs7QUFFRDtFQUNFLDBCQUF5QixFQUMxQjs7QUFFRDtFQUNFLDBCQUF5QixFQUMxQjs7QUFFRDtFQUNFLGFBQVk7RUFDWix3QkFBdUI7RUFDdkIsdUJBQXNCO0VBQ3RCLFdBQVU7RUFDVixtQ0FBa0M7RUFDbEMsaUJBQWdCO0VBQ2hCLGtCQUFpQjtFQUNqQixxQkFBb0IsRUFDckI7O0FBRUQ7RUFDRSxpQ0FBZ0MsRUFDakM7O0FBRUQ7RUFDRSxlQUFjLEVBQ2Y7O0FBRkQ7RUFDRSxlQUFjLEVBQ2Y7O0FBRkQ7RUFDRSxlQUFjLEVBQ2Y7O0FBRUQ7RUFDRSxjQUFhO0VBQ2IsZUFBYztFQUNkLG1CQUFrQixFQUNuQjs7QUFFRDtFQUNFLGVBQWMsRUFDZjs7QUFGRDtFQUNFLGVBQWMsRUFDZjs7QUFGRDtFQUNFLGVBQWMsRUFDZjs7QUFFRDtFQUNFLGVBQWMsRUFDZjs7QUFFRDtFQUNFLGVBQWMsRUFDZjs7QUFFRDtFQUNFLG1CQUFrQjtFQUNsQixhQUFZO0VBQ1osbUJBQWtCO0VBQ2xCLGFBQVk7RUFDWix1QkFBc0I7RUFDdEIsa0JBQWlCO0VBQ2pCLGlCQUFnQjtFQUNoQixtQ0FBa0M7RUFDbEMscUJBQW9CO0VBQ3BCLGlDQUFnQyxFQUNqQzs7QUFFRDtFQUNFLDJCQUEwQixFQUMzQjs7QUFFRDtFQUNFLGNBQWEsRUFDZDs7QUFFRDtFQUNFLDBCQUF5QjtFQUN6Qiw4Q0FBNkM7RUFDN0MsZUFBYyxFQUNmOztBQUVEO0VBQ0UsOENBQTZDLEVBQzlDOztBQUVEO0VBQ0UsMEJBQXlCO0VBQ3pCLDZDQUE0QztFQUM1QyxlQUFjO0VBQ2QsaUJBQWdCO0VBQ2hCLGdCQUFlO0VBQ2Ysa0JBQWlCLEVBQ2xCOztBQUVEO0VBQ0UsYUFBWTtFQUNaLFlBQVcsRUFDWjs7QUFFRDtFQUNFLDRDQUEyQyxFQUM1Qzs7QUFFRDtFQUNFLGdCQUFlLEVBQ2hCOztBQUVEO0VBQ0UsbUJBQWtCO0VBQ2xCLGlCQUFnQjtFQUNoQixhQUFZLEVBQ2I7O0FBQ0Q7RUFDRSxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFNBQVE7RUFDUixVQUFTO0VBQ1QsV0FBVTtFQUNWLGdCQUFlO0VBQ2YsZ0JBQWU7RUFDZixXQUFVO0VBQ1YseUJBQXdCLEVBQ3pCOztBQUlEO0VBQ0UsY0FBYSxFQUNkOztBQUNEO0VBQ0UsdUJBQXNCO0VBQ3RCLHNCQUFxQjtFQUNyQixrQkFBaUI7RUFDakIsZ0JBQWU7RUFDZixZQUFXO0VBQ1gsbUJBQWtCO0VBQ2xCLGdDQUErQixFQUNoQzs7QUFDRDtFQUNFLG1CQUFrQixFQUNuQjs7QUFDRDtFQUNFLG1CQUFrQixFQUNuQiIsImZpbGUiOiJzcmMvYXBwL2VkaXQtcHJvZmlsZS9lZGl0LXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpb24taW1nIHtcclxuICBtYXgtaGVpZ2h0OiAzNXZoO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuaDQge1xyXG4gIGNvbG9yOiAjMjIyNDI4O1xyXG4gIGZvbnQtc2l6ZTogMTdweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuI2ZpbGUtdXBsb2FkIHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbn1cclxuXHJcbi50aXR1bG8ge1xyXG4gIGNvbG9yOiAjZmNkMDAwO1xyXG4gIGZvbnQtc2l6ZTogMjRweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuaDEge1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5ib3RvbiBpb24tYnV0dG9uIHtcclxuICBoZWlnaHQ6IDM5cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBmb250LXNpemU6IDE3cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5zZXAge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuaDIge1xyXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gIGNvbG9yOiAjMjMyYjM4O1xyXG59XHJcblxyXG4uaW5wdXQtY2FyZCB7XHJcbiAgYm9yZGVyLXJhZGl1czogNXJlbTtcclxuICBiYWNrZ3JvdW5kOiAjZjVmNmY3O1xyXG4gIGJveC1zaGFkb3c6IDAgM3B4IDgwcHggcmdiYSgzOSwgNjgsIDc0LCAwLjIpO1xyXG59XHJcblxyXG5mb3JtLmlucHV0LWJveCB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgI2M1Y2NjZDtcclxuICBib3JkZXItcmFkaXVzOiAxcmVtO1xyXG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgdHJhbnNpdGlvbjogMC4ycyBhbGw7XHJcbn1cclxuXHJcbmZvcm0uaW5wdXQtYm94OmZvY3VzLXdpdGhpbiB7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzAyYzRkOTtcclxufVxyXG5cclxuZm9ybS5pbnB1dC1ib3g6Zm9jdXMtd2l0aGluLmVycm9yIHtcclxuICBib3JkZXI6IDJweCBzb2xpZCAjZjU0ZDNkO1xyXG59XHJcblxyXG5pbnB1dCB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gIHBhZGRpbmc6IDEuMTI1cmVtIDFyZW07XHJcbiAgd2lkdGg6IDk1JTtcclxuICBmb250LWZhbWlseTogXCJQb3BwaW5zXCIsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBmb250LXNpemU6IDEuNXJlbTtcclxuICB0cmFuc2l0aW9uOiAwLjJzIGFsbDtcclxufVxyXG5cclxuaW5wdXQ6bm90KDpsYXN0LWNoaWxkKSB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNlY2VlZWU7XHJcbn1cclxuXHJcbmlucHV0OjpwbGFjZWhvbGRlciB7XHJcbiAgY29sb3I6ICM5ZGE4YWI7XHJcbn1cclxuXHJcbmlucHV0OmZvY3VzIHtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIGNvbG9yOiAjMDgyNDJhO1xyXG4gIHBhZGRpbmc6IDJyZW0gMXJlbTtcclxufVxyXG5cclxuaW5wdXQ6Zm9jdXM6OnBsYWNlaG9sZGVyIHtcclxuICBjb2xvcjogIzc1ODU4OTtcclxufVxyXG5cclxuaW5wdXQuZXJyb3Ige1xyXG4gIGNvbG9yOiAjZjU0ZDNkO1xyXG59XHJcblxyXG5pbnB1dC5zdWNjZXNzIHtcclxuICBjb2xvcjogIzAyYzRkOTtcclxufVxyXG5cclxuLmJ1dHRvbmUge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgcGFkZGluZzogMXJlbSAzcmVtO1xyXG4gIG1hcmdpbjogMXJlbTtcclxuICBib3JkZXItcmFkaXVzOiA5OTk5OXB4O1xyXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiLCBzYW5zLXNlcmlmO1xyXG4gIHRyYW5zaXRpb246IDAuMnMgYWxsO1xyXG4gIHRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOiBlYXNlO1xyXG59XHJcblxyXG4uYnV0dG9uZTpob3ZlciB7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGV5KDNweCk7XHJcbn1cclxuXHJcbi5idXR0b25lOmZvY3VzIHtcclxuICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4uYnV0dG9uZS50ZWFsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDJjNGQ5O1xyXG4gIGJveC1zaGFkb3c6IDAgN3B4IDUwcHggcmdiYSgyLCAxOTYsIDIxNywgMC41KTtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuLmJ1dHRvbmUudGVhbDpob3ZlciB7XHJcbiAgYm94LXNoYWRvdzogMCAycHggMTBweCByZ2JhKDIsIDE5NiwgMjE3LCAwLjUpO1xyXG59XHJcblxyXG4uYnV0dG9uZS5nb29nbGUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgYm94LXNoYWRvdzogMCAzcHggMjBweCByZ2JhKDM5LCA2OCwgNzQsIDAuMik7XHJcbiAgY29sb3I6ICM1MDY1Njk7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBmb250LXNpemU6IDIycHg7XHJcbiAgbGluZS1oZWlnaHQ6IDFyZW07XHJcbn1cclxuXHJcbi5idXR0b25lLmdvb2dsZSA+IGltZyB7XHJcbiAgaGVpZ2h0OiAyMHB4O1xyXG4gIHdpZHRoOiAyMHB4O1xyXG59XHJcblxyXG4uYnV0dG9uZS5nb29nbGU6aG92ZXIge1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IDVweCByZ2JhKDM5LCA2OCwgNzQsIDAuMik7XHJcbn1cclxuXHJcbi5pb25pY29uIHtcclxuICBmb250LXNpemU6IDkwcHg7XHJcbn1cclxuXHJcbi5maWxlVXBsb2FkIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBtYXJnaW46IDEwcHg7XHJcbn1cclxuLmZpbGVVcGxvYWQgaW5wdXQudXBsb2FkIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgb3BhY2l0eTogMDtcclxuICBmaWx0ZXI6IGFscGhhKG9wYWNpdHk9MCk7XHJcbn1cclxuXHJcbi8vZXN0aWxvIGRlIGlucHV0IHRpcG8gZmlsZSBlbCBxdWUgZGViZXMgbW9kaWZpY2FyIGVzIGxhIGNsYXNlIC5jdXN0b20tZmlsZS11cGxvYWRcclxuXHJcbmlucHV0W3R5cGU9XCJmaWxlXCJdIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5jdXN0b20tZmlsZS11cGxvYWQge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHBhZGRpbmc6IDZweCAxMnB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG4uZ29vZGZvbnQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uY2lyY2xlIHtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/edit-profile/edit-profile.page.ts":
/*!***************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.page.ts ***!
  \***************************************************/
/*! exports provided: EditProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePage", function() { return EditProfilePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(rout, route, services, afs, loadingController, aut, alertController) {
        this.rout = rout;
        this.route = route;
        this.services = services;
        this.afs = afs;
        this.loadingController = loadingController;
        this.aut = aut;
        this.alertController = alertController;
    }
    EditProfilePage.prototype.ngOnInit = function () {
        this.logueado();
    };
    EditProfilePage.prototype.logueado = function () {
        var _this = this;
        this.aut.authState.subscribe(function (user) {
            if (user) {
                _this.mail = user.email;
                _this.uid = user.uid;
                console.log(_this.mail);
                _this.getProfile(_this.uid);
            }
        });
    };
    EditProfilePage.prototype.getProfile = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.services.getProfile(id).subscribe(function (data) {
                            console.log(data);
                            if (data.length !== 0) {
                                _this.cp = true;
                                _this.id = data[0].payload.doc.id;
                                _this.name = data[0].payload.doc.data().name;
                                _this.phone = data[0].payload.doc.data().phone;
                                _this.adress = data[0].payload.doc.data().adress;
                                _this.img = data[0].payload.doc.data().img;
                                _this.username = data[0].payload.doc.data().username;
                                console.log("profil full");
                            }
                            else {
                                _this.cp = false;
                                console.log("profile empty");
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.onUpload = function (e) {
        var _this = this;
        console.log(e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "image/pic_" + id;
        var ref = this.afs.ref(filePath);
        var task = this.afs.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        this.presentLoading();
        task
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return (_this.urlImage = ref.getDownloadURL()); }))
            .subscribe();
    };
    EditProfilePage.prototype.save = function (name, phone, adress, username) {
        var _this = this;
        console.log(this.cp);
        var image = this.inputimageProd.nativeElement.value;
        var data = {
            name: name,
            phone: phone,
            mail: this.mail,
            img: image || this.img,
            adress: adress,
            uid: this.uid,
            username: username || "null",
        };
        console.log(data);
        if (this.cp === false) {
            this.services.createUser(data).then(function (res) {
                console.log("Upload" + res);
                _this.messageAlert("Perfil actualizado");
            });
        }
        else {
            this.services.updateUser(data, this.id).then(function (res) {
                console.log("Upload" + res);
                _this.messageAlert("Perfil actualizado");
            });
        }
    };
    EditProfilePage.prototype.messageAlert = function (mensaje) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            message: mensaje,
                            buttons: ["OK"],
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.presentLoading = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading, _a, role, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: "Loading image",
                            duration: 2000,
                        })];
                    case 1:
                        loading = _b.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, loading.onDidDismiss()];
                    case 3:
                        _a = _b.sent(), role = _a.role, data = _a.data;
                        console.log("Loading dismissed!");
                        return [2 /*return*/];
                }
            });
        });
    };
    EditProfilePage.prototype.moveFocus = function (nextElement) {
        nextElement.setFocus();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("imageProd"),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], EditProfilePage.prototype, "inputimageProd", void 0);
    EditProfilePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-edit-profile",
            template: __webpack_require__(/*! ./edit-profile.page.html */ "./src/app/edit-profile/edit-profile.page.html"),
            styles: [__webpack_require__(/*! ./edit-profile.page.scss */ "./src/app/edit-profile/edit-profile.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_services_service__WEBPACK_IMPORTED_MODULE_6__["ServicesService"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_5__["AngularFireStorage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
    ], EditProfilePage);
    return EditProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=edit-profile-edit-profile-module.js.map