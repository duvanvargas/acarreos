(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~edit-profile-edit-profile-module~main-main-module~register-operator-register-operator-module~95db5baf"],{

/***/ "./src/app/services/services.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/services.service.ts ***!
  \**********************************************/
/*! exports provided: ServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesService", function() { return ServicesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ServicesService = /** @class */ (function () {
    function ServicesService(afs, rout) {
        this.afs = afs;
        this.rout = rout;
        this.anuncios = [];
        this.info = [];
    }
    ServicesService.prototype.validarDireccion = function (dir) {
        return __awaiter(this, void 0, void 0, function () {
            var direccion;
            return __generator(this, function (_a) {
                direccion = encodeURIComponent(dir + ",Bogotá ,Colombia");
                return [2 /*return*/, fetch("https://us1.locationiq.com/v1/search.php?key=pk.46e66934c4e815acabec7b25ec109eb2&q=" +
                        direccion +
                        "&format=json&limit=1")];
            });
        });
    };
    ServicesService.prototype.goto = function (id) {
        this.rout.navigateByUrl(id);
    };
    // User stuff
    ServicesService.prototype.getProfile = function (id) {
        var _this = this;
        this.itemsCollection = this.afs.collection("users/" + id + "/profile/");
        return this.itemsCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (info) {
            _this.info = [];
            for (var _i = 0, info_1 = info; _i < info_1.length; _i++) {
                var infos = info_1[_i];
                _this.info.unshift(infos);
            }
            return _this.info;
        }));
    };
    ServicesService.prototype.createUser = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afs.collection("users/" + value.uid + "/profile").add({
                name: value.name,
                phone: value.phone,
                mail: value.mail,
                img: value.img,
                uid: value.uid,
                adress: value.adress,
                date: Date.now(),
                username: value.username,
                type: "client",
            });
            _this.rout.navigateByUrl("profile");
        });
    };
    ServicesService.prototype.createOperator = function (value) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afs.collection("users/" + value.uid + "/profile").add({
                name: value.name,
                phone: value.phone,
                mail: value.mail,
                img: value.img,
                uid: value.uid,
                adress: value.adress,
                date: Date.now(),
                username: value.username,
                type: "operator",
                license_plate: value.license_plate,
                type_car: value.type_car,
                soat: value.soat,
                type_license: value.type_license,
                tecno: value.tecno,
            });
            _this.rout.navigateByUrl("profile");
        });
    };
    ServicesService.prototype.createAcarreo = function (formData) {
        var _this = this;
        // uniq generetad id
        var id = Math.random().toString(36).substring(2);
        return new Promise(function (resolve, reject) {
            _this.afs.collection("trips").doc(id).set(formData);
            _this.rout.navigateByUrl("/tabs/service/" + id);
        });
    };
    ServicesService.prototype.acarreoDetail = function (id) {
        var docRef = this.afs.collection("trips").doc(id);
        return new Promise(function (resolve, reject) {
            docRef.ref
                .get()
                .then(function (doc) {
                if (doc.exists) {
                    resolve(doc.data());
                }
                else {
                    reject([]);
                }
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServicesService.prototype.suscribeLastAcarreos = function (id) {
        var _this = this;
        this.itemsCollection = this.afs.collection("trips");
        return this.itemsCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (info) {
            _this.info = [];
            for (var _i = 0, info_2 = info; _i < info_2.length; _i++) {
                var infos = info_2[_i];
                _this.info.unshift(infos);
            }
            return _this.info;
        }));
    };
    ServicesService.prototype.suscribeAcarreo = function (id) {
        return this.afs
            .collection("trips")
            .doc(id)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (action) {
            var data = action.payload.data();
            var id = action.payload.id;
            return __assign({ id: id }, data);
        }));
    };
    ServicesService.prototype.getAllAcarreos = function (idUser) {
        var docRef = this.afs.collection("trips", function (ref) {
            return ref.where("uid", "==", idUser);
        });
        return new Promise(function (resolve, reject) {
            docRef.ref
                .get()
                .then(function (res) {
                resolve(res);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServicesService.prototype.getAllStaticAcarreos = function (idUser) {
        var docRef = this.afs.collection("trips");
        return new Promise(function (resolve, reject) {
            docRef.ref
                .get()
                .then(function (res) {
                console.warn(res);
                resolve(res);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServicesService.prototype.updateUser = function (value, id) {
        return this.afs
            .collection("users")
            .doc(value.uid)
            .collection("profile")
            .doc(id)
            .set(value);
    };
    ServicesService.prototype.updateTrip = function (value) {
        console.warn({ value: value });
        return this.afs.collection("trips").doc(value.id).ref.update(value);
    };
    // Entry stuff
    ServicesService.prototype.AddEntry = function (description) {
        var _this = this;
        // uniq generetad id
        var id = Math.random().toString(36).substring(2);
        return new Promise(function (resolve, reject) {
            _this.afs.collection("entrys").doc(id).set({
                description: description,
                id: id,
                date: Date.now(),
            });
            _this.rout.navigateByUrl("profile");
        });
    };
    ServicesService.prototype.getEntrys = function () {
        var _this = this;
        this.itemsCollection = this.afs.collection("entrys");
        return this.itemsCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (info) {
            _this.info = [];
            for (var _i = 0, info_3 = info; _i < info_3.length; _i++) {
                var infos = info_3[_i];
                _this.info.unshift(infos);
            }
            return _this.info;
        }));
    };
    ServicesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: "root",
        }),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ServicesService);
    return ServicesService;
}());



/***/ })

}]);
//# sourceMappingURL=default~edit-profile-edit-profile-module~main-main-module~register-operator-register-operator-module~95db5baf.js.map