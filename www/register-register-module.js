(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["register-register-module"],{

/***/ "./src/app/register/register.module.ts":
/*!*********************************************!*\
  !*** ./src/app/register/register.module.ts ***!
  \*********************************************/
/*! exports provided: RegisterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register.page */ "./src/app/register/register.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_5__["RegisterPage"]
    }
];
var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_register_page__WEBPACK_IMPORTED_MODULE_5__["RegisterPage"]]
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());



/***/ }),

/***/ "./src/app/register/register.page.html":
/*!*********************************************!*\
  !*** ./src/app/register/register.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding text-center>\r\n  <div class=\"form\">\r\n    <img src=\"assets/img/logoicon.png\" class=\"login-image\" />\r\n    <h1>Regístrate aquí</h1>\r\n    <p>Gratis. Sin necesidad de tarjeta de crédito</p>\r\n    <ion-list>\r\n      <ion-item no-lines>\r\n        <ion-label class=\"labelTitle\" position=\"floating\"\r\n          >Nombres y apellidos</ion-label\r\n        >\r\n        <ion-input\r\n          class=\"inputForm\"\r\n          type=\"text\"\r\n          placeholder=\"Nombres y apellidos\"\r\n          (keyup.enter)=\"moveFocus(b)\"\r\n          [(ngModel)]=\"name\"\r\n        ></ion-input>\r\n      </ion-item>\r\n      <br />\r\n      <ion-item no-lines>\r\n        <ion-label class=\"labelTitle\" position=\"floating\">Dirección</ion-label>\r\n        <ion-input\r\n          class=\"inputForm\"\r\n          type=\"text\"\r\n          placeholder=\"Dirección\"\r\n          (keyup.enter)=\"moveFocus(b)\"\r\n          [(ngModel)]=\"address\"\r\n        ></ion-input>\r\n      </ion-item>\r\n      <br />\r\n      <ion-item no-lines>\r\n        <ion-label class=\"labelTitle\" position=\"floating\">Celular</ion-label>\r\n        <ion-input\r\n          class=\"inputForm\"\r\n          type=\"text\"\r\n          placeholder=\"Celular\"\r\n          (keyup.enter)=\"moveFocus(b)\"\r\n          [(ngModel)]=\"phone\"\r\n        ></ion-input>\r\n      </ion-item>\r\n      <br />\r\n      <ion-item no-lines>\r\n        <ion-label class=\"labelTitle\" position=\"floating\">Email</ion-label>\r\n        <ion-input\r\n          class=\"inputForm\"\r\n          type=\"text\"\r\n          placeholder=\"Email\"\r\n          (keyup.enter)=\"moveFocus(b)\"\r\n          [(ngModel)]=\"email\"\r\n        ></ion-input>\r\n      </ion-item>\r\n      <br />\r\n      <ion-item no-lines>\r\n        <ion-label class=\"labelTitle\" position=\"floating\">Contraseña</ion-label>\r\n        <ion-input\r\n          class=\"inputForm\"\r\n          [type]=\"passwordType\"\r\n          #b\r\n          type=\"password\"\r\n          (keyup.enter)=\"moveFocus(c)\"\r\n          placeholder=\"Contraseña\"\r\n          [(ngModel)]=\"password\"\r\n        ></ion-input>\r\n      </ion-item>\r\n      <br />\r\n      <ion-item no-lines>\r\n        <ion-label class=\"labelTitle\" position=\"floating\"\r\n          >Repetir contraseña</ion-label\r\n        >\r\n        <ion-input\r\n          class=\"inputForm\"\r\n          (keyup.enter)=\"register()\"\r\n          [type]=\"passwordType\"\r\n          #c\r\n          placeholder=\"Repetir contraseña\"\r\n          type=\"password\"\r\n          [(ngModel)]=\"cpassword\"\r\n        ></ion-input>\r\n      </ion-item>\r\n    </ion-list>\r\n    <br />\r\n    <ion-button\r\n      class=\"goodfont boton\"\r\n      mode=\"ios\"\r\n      fill=\"solid\"\r\n      expand=\"block\"\r\n      color=\"primary\"\r\n      [disabled]=\"\r\n      email.trim().length === 0 ||\r\npassword.trim().length === 0 ||\r\ncpassword.trim().length === 0 ||\r\nname.trim().length === 0 ||\r\nphone.trim().length === 0\r\n      \"\r\n      (click)=\"register()\"\r\n      >Registrar</ion-button\r\n    >\r\n    <br />\r\n    <p class=\"register-text\" (click)=\"goLogin()\">\r\n      ¿Ya tienes cuenta? <strong><a> Ingresa aquí</a></strong>\r\n    </p>\r\n  </div>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/register/register.page.scss":
/*!*********************************************!*\
  !*** ./src/app/register/register.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/register/register.page.ts":
/*!*******************************************!*\
  !*** ./src/app/register/register.page.ts ***!
  \*******************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var RegisterPage = /** @class */ (function () {
    function RegisterPage(afr, rout, services, alertController) {
        this.afr = afr;
        this.rout = rout;
        this.services = services;
        this.alertController = alertController;
        this.email = "";
        this.password = "";
        this.cpassword = "";
        this.name = "";
        this.address = "";
        this.phone = "";
        this.passwordType = "password";
        this.passwordIcon = "eye-off";
    }
    RegisterPage.prototype.register = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, email, password, cpassword, error_1;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this, email = _a.email, password = _a.password, cpassword = _a.cpassword;
                        if (!(password !== cpassword)) return [3 /*break*/, 1];
                        this.errorpassIguales();
                        this.rout.navigate(["/register"]);
                        return [3 /*break*/, 4];
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.afr.auth
                                .createUserWithEmailAndPassword(email, password)
                                .then(function (data) {
                                //Registar información personal
                                var dataUser = {
                                    name: _this.name,
                                    phone: _this.phone,
                                    mail: _this.email,
                                    adress: _this.address,
                                    uid: data.user.uid,
                                    username: "null",
                                    tokenPush: localStorage.getItem("userId") || "-",
                                    img: "null",
                                };
                                _this.services.createUser(dataUser).then(function (res) {
                                    _this.rout.navigateByUrl("");
                                });
                            })];
                    case 2:
                        _b.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _b.sent();
                        console.log(error_1);
                        if (error_1.code === "auth/wrong-password") {
                            this.error("Contraseña invalida");
                        }
                        if (error_1.code === "auth/user-not-found") {
                            this.error("Usuario no encontrado");
                        }
                        if (error_1.code === "auth/email-already-in-use") {
                            this.error("Usuario ya existente");
                        }
                        if (error_1.code === "auth/argument-error") {
                            this.error("Argument error");
                        }
                        if (error_1.code === "auth/invalid-email") {
                            this.error("Correo invalido");
                        }
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.goLogin = function () {
        this.rout.navigate(["/login"]);
    };
    RegisterPage.prototype.errorpassIguales = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            message: "Las contraseñas no son iguales",
                            buttons: ["OK"],
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.errorServ = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            message: "Algo ocurrió , intenta mas tarde",
                            buttons: ["OK"],
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.presentLoading = function (loading) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, loading.present()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    RegisterPage.prototype.error = function (mensaje) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            message: mensaje,
                            buttons: ["OK"],
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === "text" ? "password" : "text";
        this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
    };
    RegisterPage.prototype.moveFocus = function (nextElement) {
        nextElement.setFocus();
    };
    RegisterPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-register",
            template: __webpack_require__(/*! ./register.page.html */ "./src/app/register/register.page.html"),
            styles: [__webpack_require__(/*! ./register.page.scss */ "./src/app/register/register.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
    ], RegisterPage);
    return RegisterPage;
}());



/***/ })

}]);
//# sourceMappingURL=register-register-module.js.map