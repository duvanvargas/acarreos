(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: "",
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_5__["TabsPage"],
        children: [
            {
                path: "profile",
                children: [
                    {
                        path: "",
                        loadChildren: function () {
                            return Promise.all(/*! import() */[__webpack_require__.e(11), __webpack_require__.e("common")]).then(__webpack_require__.bind(null, /*! ../edit-profile/edit-profile.module */ "./src/app/edit-profile/edit-profile.module.ts")).then(function (m) { return m.EditProfilePageModule; });
                        },
                    },
                ],
            },
            {
                path: "stats",
                children: [
                    {
                        path: "",
                        loadChildren: function () {
                            return Promise.all(/*! import() */[__webpack_require__.e(9), __webpack_require__.e("common")]).then(__webpack_require__.bind(null, /*! ../stats/stats.module */ "./src/app/stats/stats.module.ts")).then(function (m) { return m.StatsPageModule; });
                        },
                    },
                ],
            },
            {
                path: "trips",
                children: [
                    {
                        path: "",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ "common").then(__webpack_require__.bind(null, /*! ../trips/trips.module */ "./src/app/trips/trips.module.ts")).then(function (m) { return m.TripsPageModule; });
                        },
                    },
                ],
            },
            {
                path: "logout",
                children: [
                    {
                        path: "",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ "logout-logout-module").then(__webpack_require__.bind(null, /*! ../logout/logout.module */ "./src/app/logout/logout.module.ts")).then(function (m) { return m.LogoutPageModule; });
                        },
                    },
                ],
            },
            {
                path: "settings",
                children: [
                    {
                        path: "",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ "settings-settings-module").then(__webpack_require__.bind(null, /*! ../settings/settings.module */ "./src/app/settings/settings.module.ts")).then(function (m) { return m.SettingsPageModule; });
                        },
                    },
                ],
            },
            {
                path: "service",
                children: [
                    {
                        path: "",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ "service-service-module").then(__webpack_require__.bind(null, /*! ../service/service.module */ "./src/app/service/service.module.ts")).then(function (m) { return m.ServicePageModule; });
                        },
                    },
                    {
                        path: "formulario",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ 13).then(__webpack_require__.bind(null, /*! ../formulario/formulario.module */ "./src/app/formulario/formulario.module.ts")).then(function (m) { return m.FormularioPageModule; });
                        },
                    },
                    {
                        path: "formularioMudanza",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ 12).then(__webpack_require__.bind(null, /*! ../formularioMudanza/formularioMudanza.module */ "./src/app/formularioMudanza/formularioMudanza.module.ts")).then(function (m) { return m.FormularioMudanzaPageModule; });
                        },
                    },
                    {
                        path: "formularioMudanza",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ 12).then(__webpack_require__.bind(null, /*! ../formularioMudanza/formularioMudanza.module */ "./src/app/formularioMudanza/formularioMudanza.module.ts")).then(function (m) { return m.FormularioMudanzaPageModule; });
                        },
                    },
                    {
                        path: "formularioGrua",
                        loadChildren: function () {
                            return __webpack_require__.e(/*! import() */ 62).then(__webpack_require__.bind(null, /*! ../formularioGrua/formularioGrua.module */ "./src/app/formularioGrua/formularioGrua.module.ts")).then(function (m) { return m.FormularioGruaPageModule; });
                        },
                    },
                    {
                        path: ":myServiceId",
                        loadChildren: function () {
                            return Promise.all(/*! import() */[__webpack_require__.e(10), __webpack_require__.e("common")]).then(__webpack_require__.bind(null, /*! ../service-detail/service-detail.module */ "./src/app/service-detail/service-detail.module.ts")).then(function (m) { return m.ServiceDetailPageModule; });
                        },
                    },
                ],
            },
            {
                path: "",
                redirectTo: "/tabs/profile",
                pathMatch: "full",
            },
        ],
    },
    {
        path: "",
        redirectTo: "/tabs/profile",
        pathMatch: "full",
    },
];
var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_5__["TabsPage"]],
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/tabs/tabs.page.html":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\n  <ion-tab-bar slot=\"top\">\n    <ion-tab-button tab=\"profile\">\n      <ion-icon name=\"person\"></ion-icon>\n    </ion-tab-button>\n    <ion-tab-button\n      tab=\"stats\"\n      *ngIf=\"this.userDATA && this.userDATA.type === 'operator'\"\n    >\n      <ion-icon name=\"stats\"></ion-icon>\n    </ion-tab-button>\n    <ion-tab-button tab=\"service\">\n      <ion-icon name=\"car\"></ion-icon>\n    </ion-tab-button>\n    <ion-tab-button tab=\"trips\">\n      <ion-icon name=\"calendar\"></ion-icon>\n    </ion-tab-button>\n    <!--<ion-tab-button tab=\"settings\">\n      <ion-icon name=\"settings\"></ion-icon>\n    </ion-tab-button>-->\n    <ion-tab-button tab=\"logout\">\n      <ion-icon name=\"log-out\"></ion-icon>\n    </ion-tab-button>\n    <ion-tab-button tab=\"settings\" class=\"hiddenTab\">\n      <ion-icon name=\"calendar\"></ion-icon>\n      <ion-label>Ajustes</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"settings\" class=\"hiddenTab\">\n      <ion-icon name=\"calendar\"></ion-icon>\n      <ion-label>Ajustes</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"settings\" class=\"hiddenTab\">\n      <ion-icon name=\"calendar\"></ion-icon>\n      <ion-label>Ajustes</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n\n  <div class=\"whatsapp\">\n    <a\n      target=\"_blank\"\n      href=\"https://web.whatsapp.com/send?phone=573112527551&text=Hola%20estoy%20interesado%20en%20un%20servicio.\"\n    >\n      <img src=\"assets/img/whats.svg\" />\n    </a>\n  </div>\n</ion-tabs>\n"

/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hiddenTab {\n  pointer-events: none;\n  opacity: 0; }\n\n.whatsapp {\n  position: absolute;\n  height: 70px;\n  width: 70px;\n  background: #10dc60;\n  bottom: 10px;\n  right: 10px;\n  z-index: 9999999;\n  border-radius: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFicy9DOlxcVXNlcnNcXEFsZXhhbmRlclZhcmdhc1xcRG9jdW1lbnRzXFxkZXZlbG9wbWVudHN5c1xcYWNhcnJlb3Mvc3JjXFxhcHBcXHRhYnNcXHRhYnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQW9CO0VBQ3BCLFdBQVUsRUFDWDs7QUFFRDtFQUNFLG1CQUFrQjtFQUNsQixhQUFZO0VBQ1osWUFBVztFQUNYLG9CQUFtQjtFQUNuQixhQUFZO0VBQ1osWUFBVztFQUNYLGlCQUFnQjtFQUNoQixtQkFBa0IsRUFDbkIiLCJmaWxlIjoic3JjL2FwcC90YWJzL3RhYnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpZGRlblRhYiB7XHJcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgb3BhY2l0eTogMDtcclxufVxyXG5cclxuLndoYXRzYXBwIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgaGVpZ2h0OiA3MHB4O1xyXG4gIHdpZHRoOiA3MHB4O1xyXG4gIGJhY2tncm91bmQ6ICMxMGRjNjA7XHJcbiAgYm90dG9tOiAxMHB4O1xyXG4gIHJpZ2h0OiAxMHB4O1xyXG4gIHotaW5kZXg6IDk5OTk5OTk7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var TabsPage = /** @class */ (function () {
    function TabsPage(servicesService, aut, rout, alertController, loadingController) {
        this.servicesService = servicesService;
        this.aut = aut;
        this.rout = rout;
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.firstload = true;
    }
    TabsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.aut.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (user) {
            if (user) {
                _this.getProfile(user.uid);
            }
        });
    };
    TabsPage.prototype.getProfile = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.servicesService
                            .getProfile(id)
                            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1))
                            .subscribe(function (data) {
                            _this.userDATA = data[0].payload.doc.data();
                            if (_this.userDATA.type === "operator") {
                                _this.servicesService.suscribeLastAcarreos(id).subscribe(function (res) {
                                    res.forEach(function (element) {
                                        var data = element.payload.doc.data();
                                        data.id = element.payload.doc.id;
                                        if (data.driverId === 0 &&
                                            parseInt(_this.userDATA.type_car) === parseInt(data.tipoVehiculo)) {
                                            _this.firstload = false;
                                            _this.presentAlertConfirm(data);
                                        }
                                    });
                                });
                            }
                            //type_car para despues filtrar por servicios de mi categoria
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TabsPage.prototype.presentAlertConfirm = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            cssClass: "my-custom-class",
                            header: "Viaje nuevo",
                            message: "¿Deseas aceptar un nuevo viaje?",
                            buttons: [
                                {
                                    text: "Cancelar",
                                    role: "cancel",
                                    cssClass: "secondary",
                                    handler: function (blah) {
                                        console.log("Cancelar Okay");
                                    },
                                },
                                {
                                    text: "Aceptar",
                                    handler: function () {
                                        _this.servicesService
                                            .updateTrip(__assign({}, data, { status: "TAKEN", driverId: _this.userDATA.uid }))
                                            .then(function (res) {
                                            console.log(res);
                                            _this.rout.navigateByUrl("/tabs/trips");
                                            window.alert("Puedes consultar todos tus viajes aquí");
                                        });
                                        console.log("Confirm Okay");
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TabsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-tabs",
            template: __webpack_require__(/*! ./tabs.page.html */ "./src/app/tabs/tabs.page.html"),
            styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")]
        }),
        __metadata("design:paramtypes", [_services_services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], TabsPage);
    return TabsPage;
}());



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module.js.map