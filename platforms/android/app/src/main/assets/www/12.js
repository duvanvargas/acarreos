(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "./src/app/formularioMudanza/formularioMudanza.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/formularioMudanza/formularioMudanza.module.ts ***!
  \***************************************************************/
/*! exports provided: FormularioMudanzaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioMudanzaPageModule", function() { return FormularioMudanzaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _formularioMudanza_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./formularioMudanza.page */ "./src/app/formularioMudanza/formularioMudanza.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: "",
        component: _formularioMudanza_page__WEBPACK_IMPORTED_MODULE_5__["FormularioMudanzaPage"],
    },
];
var FormularioMudanzaPageModule = /** @class */ (function () {
    function FormularioMudanzaPageModule() {
    }
    FormularioMudanzaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_formularioMudanza_page__WEBPACK_IMPORTED_MODULE_5__["FormularioMudanzaPage"]],
        })
    ], FormularioMudanzaPageModule);
    return FormularioMudanzaPageModule;
}());



/***/ }),

/***/ "./src/app/formularioMudanza/formularioMudanza.page.html":
/*!***************************************************************!*\
  !*** ./src/app/formularioMudanza/formularioMudanza.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button [routerLink]=\"['/tabs/service']\">\n        <ion-icon\n          mode=\"ios\"\n          color=\"primary\"\n          slot=\"icon-only\"\n          name=\"arrow-back\"\n        ></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title text-center>Formulario</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content scroll=\"false\">\n  <div padding>\n    <img src=\"assets/img/logoicon.png\" class=\"login-image\" />\n    <h1>Mudanza</h1>\n    <p text-center>\n      <strong>Por favor ingrese los datos para su servicio</strong>\n    </p>\n\n    <ion-list margin-top margin-bottom>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Fecha recogida</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"date\"\n          placeholder=\"Fecha recogida\"\n          [(ngModel)]=\"formAcarreo.fechaRecogida\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Tipo de vehículo</ion-label\n        >\n        <div class=\"cont_vehiculos\">\n          <!--<div *ngIf=\"formAcarreo.tipoVehiculo === 1\">\n            <strong><h1>Tipo Carry</h1></strong>\n            <img src=\"assets/img/Carri.jpg\" width=\"330px\" />\n            <strong><p>2 metros cúbicos - 700 Kilogramos</p></strong>\n          </div>-->\n          <div *ngIf=\"formAcarreo.tipoVehiculo === 2\">\n            <strong><h1>Camioneta tipo LUV</h1></strong>\n            <img src=\"assets/img/luv.jpeg\" width=\"330px\" />\n            <strong><p>8 metros cubicos - 1.000 Kilogramos</p></strong>\n          </div>\n          <div *ngIf=\"formAcarreo.tipoVehiculo === 3\">\n            <strong> <h1>Tipo NHR (150)</h1></strong>\n            <img src=\"assets/img/nhr.png\" width=\"330px\" />\n            <strong><p>2 Toneladas - 15 metros cúbicos</p></strong>\n          </div>\n          <div *ngIf=\"formAcarreo.tipoVehiculo === 4\">\n            <strong> <h1>Tipo NKR (250)</h1></strong>\n            <img src=\"assets/img/NKR.png\" width=\"330px\" />\n            <strong><p>3 Toneladas - 28 metros cúbicos</p></strong>\n          </div>\n          <div *ngIf=\"formAcarreo.tipoVehiculo === 5\">\n            <strong><h1>Tipo NPR (450)</h1></strong>\n            <img src=\"assets/img/NPR.jpg\" width=\"330px\" />\n            <strong><p>4 Toneladas - 36 metros cùbicos</p></strong>\n          </div>\n          <div *ngIf=\"formAcarreo.tipoVehiculo === 6\">\n            <strong><h1>Tipo FRR (600)</h1></strong>\n            <img src=\"assets/img/FRR.jpg\" width=\"330px\" />\n            <strong><p>5 Toneladas</p></strong>\n          </div>\n        </div>\n      </ion-item>\n      <ion-item>\n        <div class=\"cont_button_vehiculo\">\n          <ion-button\n            [disabled]=\"formAcarreo.tipoVehiculo === 2\"\n            class=\"goodfont boton\"\n            margin-bottom\n            mode=\"ios\"\n            (click)=\"prev()\"\n            >Anterior</ion-button\n          >\n          <ion-button\n            [disabled]=\"formAcarreo.tipoVehiculo === 6\"\n            class=\"goodfont boton\"\n            margin-bottom\n            mode=\"ios\"\n            (click)=\"next()\"\n            >Siguiente</ion-button\n          >\n        </div>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\" class=\"labelTitle\"\n          >Dirección Origen</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Dirección Origen\"\n          [(ngModel)]=\"formAcarreo.dirRecogida\"\n          [ngClass]=\"{'invalid':formAcarreo.dirRecogida=== ''}\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >(piso, apto , bodega )</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Complemento ( Piso, Apto, Oficina)\"\n          [(ngModel)]=\"formAcarreo.dirRecogidaExtra\"\n          [ngClass]=\"{'invalid':formAcarreo.dirRecogidaExtra=== ''}\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Dirección Destino</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Dirección Destino\"\n          [ngClass]=\"{'invalid':formAcarreo.dirDestino=== ''}\"\n          [(ngModel)]=\"formAcarreo.dirDestino\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >(piso, apto , bodega )</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Complemento ( Piso, Apto, Oficina)\"\n          [(ngModel)]=\"formAcarreo.dirDestinoExtra\"\n          [ngClass]=\"{'invalid':formAcarreo.dirDestinoExtra=== ''}\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label class=\"labelTitle\" position=\"stacked\"\n          >Objetos a transportar</ion-label\n        >\n        <ion-input\n          class=\"inputForm\"\n          type=\"text\"\n          placeholder=\"Objetos a transportar\"\n          [ngClass]=\"{'invalid':formAcarreo.objetosTrastear=== ''}\"\n          [(ngModel)]=\"formAcarreo.objetosTrastear\"\n          required\n        ></ion-input>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label>Ascensor</ion-label>\n        <ion-checkbox\n          slot=\"end\"\n          [(ngModel)]=\"formAcarreo.ascensor\"\n        ></ion-checkbox>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-label>Escalera</ion-label>\n        <ion-checkbox\n          slot=\"end\"\n          [(ngModel)]=\"formAcarreo.escalera\"\n        ></ion-checkbox>\n      </ion-item>\n    </ion-list>\n    <ion-button\n      [disabled]=\"!checkForm()\"\n      class=\"goodfont boton\"\n      margin-bottom\n      mode=\"ios\"\n      expand=\"block\"\n      (click)=\"crearViaje()\"\n      >SOLICITAR</ion-button\n    >\n    <br />\n    <br />\n    <br />\n    <br />\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/formularioMudanza/formularioMudanza.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/formularioMudanza/formularioMudanza.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".payment {\n  display: flex;\n  margin: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybXVsYXJpb011ZGFuemEvQzpcXFVzZXJzXFxBbGV4YW5kZXJWYXJnYXNcXERvY3VtZW50c1xcZGV2ZWxvcG1lbnRzeXNcXGFjYXJyZW9zL3NyY1xcYXBwXFxmb3JtdWxhcmlvTXVkYW56YVxcZm9ybXVsYXJpb011ZGFuemEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYTtFQUNiLGFBQVksRUFDYiIsImZpbGUiOiJzcmMvYXBwL2Zvcm11bGFyaW9NdWRhbnphL2Zvcm11bGFyaW9NdWRhbnphLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYXltZW50IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIG1hcmdpbjogYXV0bztcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/formularioMudanza/formularioMudanza.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/formularioMudanza/formularioMudanza.page.ts ***!
  \*************************************************************/
/*! exports provided: FormularioMudanzaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormularioMudanzaPage", function() { return FormularioMudanzaPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var FormularioMudanzaPage = /** @class */ (function () {
    function FormularioMudanzaPage(servicesService, aut, loadingController) {
        this.servicesService = servicesService;
        this.aut = aut;
        this.loadingController = loadingController;
        /**
         * FECHA RECOGIDA
              DIMENSIONES
              UBICACIÓN
              DISTANCIA ENTRE CAMION Y LA PUERTA
              DIRECCION RECOGIDA
              DIRECCIÓN DESTINO
              CONDICIONES SERVICIO
         */
        this.slideOpts = {
            initialSlide: 1,
            speed: 400,
        };
        this.formAcarreo = {
            tipo: "Mudanza",
            date: Date.now(),
            status: "PENDING",
            userId: null,
            tipoVehiculo: 2,
            driverId: 0,
            fechaRecogida: null,
            dimensiones: {
                alto: null,
                ancho: null,
                largo: null,
            },
            dirRecogida: "",
            dirRecogidaExtra: "",
            dirDestino: "",
            dirDestinoExtra: "",
            anotaciones: "",
            distanciaCamion: "",
            objetosTrastear: "",
            cantObjetos: "",
            interior: "",
            escalera: false,
            ascensor: false,
        };
    }
    FormularioMudanzaPage.prototype.next = function () {
        this.formAcarreo.tipoVehiculo++;
    };
    FormularioMudanzaPage.prototype.prev = function () {
        this.formAcarreo.tipoVehiculo--;
    };
    FormularioMudanzaPage.prototype.ngOnInit = function () {
        var _this = this;
        this.aut.authState.subscribe(function (user) {
            if (user) {
                _this.userLogged = user;
            }
        });
    };
    FormularioMudanzaPage.prototype.crearViaje = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.servicesService
                    .validarDireccion(this.formAcarreo.dirRecogida)
                    .then(function (resRaw) { return resRaw.json(); })
                    .then(function (res) {
                    console.warn(res);
                    if (res.error) {
                        alert("La direccion de recogida no es valida");
                        return;
                    }
                    if (res && !res.error && res.length === 0) {
                        alert("La direccion de recogida no es valida");
                    }
                    else {
                        _this.servicesService
                            .validarDireccion(_this.formAcarreo.dirDestino)
                            .then(function (resRaw2) { return resRaw2.json(); })
                            .then(function (res2) {
                            console.warn(res2);
                            if (res.error) {
                                alert("La direccion de destino no es valida");
                                return;
                            }
                            if (res2 && !res.error && res2.length === 0) {
                                alert("La direccion de destino no es valida");
                            }
                            else {
                                _this.generarViaje();
                            }
                        });
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    FormularioMudanzaPage.prototype.generarViaje = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.formAcarreo.userId = this.userLogged.uid;
                        console.log(this.formAcarreo);
                        return [4 /*yield*/, this.loadingController.create({
                                message: "Creando viaje...",
                                duration: 2000,
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.servicesService.createAcarreo(this.formAcarreo);
                        return [2 /*return*/];
                }
            });
        });
    };
    FormularioMudanzaPage.prototype.checkForm = function () {
        var form = this.formAcarreo;
        return (this.isValid(form.fechaRecogida) &&
            //this.isValid(form.dimensiones.alto) &&
            //this.isValid(form.dimensiones.ancho) &&
            //this.isValid(form.dimensiones.largo) &&
            this.isValid(form.dirRecogida) &&
            this.isValid(form.dirDestino));
    };
    FormularioMudanzaPage.prototype.isValid = function (field) {
        if (field === "")
            return false;
        if (field === null)
            return false;
        return true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("slides"),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
    ], FormularioMudanzaPage.prototype, "slides", void 0);
    FormularioMudanzaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-formulario",
            template: __webpack_require__(/*! ./formularioMudanza.page.html */ "./src/app/formularioMudanza/formularioMudanza.page.html"),
            styles: [__webpack_require__(/*! ./formularioMudanza.page.scss */ "./src/app/formularioMudanza/formularioMudanza.page.scss")]
        }),
        __metadata("design:paramtypes", [_services_services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], FormularioMudanzaPage);
    return FormularioMudanzaPage;
}());



/***/ })

}]);
//# sourceMappingURL=12.js.map