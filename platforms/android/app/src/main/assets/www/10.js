(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./src/app/safe.pipe.ts":
/*!******************************!*\
  !*** ./src/app/safe.pipe.ts ***!
  \******************************/
/*! exports provided: SafePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SafePipe", function() { return SafePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: "safe" }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["DomSanitizer"]])
    ], SafePipe);
    return SafePipe;
}());



/***/ }),

/***/ "./src/app/service-detail/service-detail.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/service-detail/service-detail.module.ts ***!
  \*********************************************************/
/*! exports provided: ServiceDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceDetailPageModule", function() { return ServiceDetailPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _safe_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../safe.pipe */ "./src/app/safe.pipe.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _service_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service-detail.page */ "./src/app/service-detail/service-detail.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: "",
        component: _service_detail_page__WEBPACK_IMPORTED_MODULE_6__["ServiceDetailPage"],
    },
];
var ServiceDetailPageModule = /** @class */ (function () {
    function ServiceDetailPageModule() {
    }
    ServiceDetailPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            providers: [_safe_pipe__WEBPACK_IMPORTED_MODULE_4__["SafePipe"]],
            declarations: [_service_detail_page__WEBPACK_IMPORTED_MODULE_6__["ServiceDetailPage"], _safe_pipe__WEBPACK_IMPORTED_MODULE_4__["SafePipe"]],
        })
    ], ServiceDetailPageModule);
    return ServiceDetailPageModule;
}());



/***/ }),

/***/ "./src/app/service-detail/service-detail.page.html":
/*!*********************************************************!*\
  !*** ./src/app/service-detail/service-detail.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title text-center>Servicio</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content scroll=\"false\">\n  <div padding *ngIf=\"serviceInfo\">\n    <iframe\n      *ngIf=\"serviceInfo.dirRecogida && serviceInfo.dirDestino\"\n      [src]=\"'https://www.google.com/maps/embed/v1/directions?key=AIzaSyB1KLpJ1-CMeux7HEZZnO_CNdtfr7o0cWI&origin='+encodeURL(serviceInfo.dirRecogida +' '+serviceInfo.dirRecogidaExtra)+ ', Colombia&destination='+ encodeURL(serviceInfo.dirDestino + ' ' + serviceInfo.dirDestinoExtra) + ', Colombia&avoid=tolls|highways&zoom=12'  | safe\"\n      title=\"W3Schools Free Online Web Tutorials\"\n    >\n    </iframe>\n    <p text-center>Tu código de transacción es:</p>\n    <p text-center>\n      <ion-icon name=\"arrow-up\"></ion-icon>\n    </p>\n    <p text-center># {{myServiceId}}</p>\n    <p text-center>\n      <ion-icon name=\"pin\"></ion-icon>\n    </p>\n    <div class=\"address-container\">\n      <div>\n        <p>Origen</p>\n        <p><strong>{{ serviceInfo.dirRecogida }}</strong></p>\n      </div>\n      <div>\n        <p>Destino:</p>\n        <p><strong>{{ serviceInfo.dirDestino }}</strong></p>\n      </div>\n    </div>\n\n    <div class=\"driver-card\" *ngIf=\"serviceInfo.status === 'PENDING'\">\n      <img src=\"assets/img/acarreos.PNG\" alt=\"\" />\n      <div class=\"detail-user\">\n        <strong>Esperando conductor...</strong>\n      </div>\n      <div class=\"blue\"></div>\n    </div>\n    <div class=\"driver-card\" *ngIf=\"serviceInfo.status === 'TAKEN'\">\n      <img src=\"assets/img/acarreos.PNG\" alt=\"\" />\n      <div class=\"detail-user\">\n        <strong>Nombre :</strong>{{driverInfo.name}}\n        <br />\n        <strong>Telefono :</strong>{{driverInfo.phone}}\n        <p>5 trayectos</p>\n      </div>\n      <div class=\"blue\">Acarreos</div>\n    </div>\n    <h1></h1>\n    <div class=\"info_driver\">\n      <ion-card>\n        <ion-card-header>\n          <ion-card-title\n            ><strong>Información del Vehículo </strong></ion-card-title\n          >\n        </ion-card-header>\n\n        <ion-card-content>\n          <strong>Tecnomecanica :</strong>{{driverInfo.tecno}}<br />\n          <strong>SOAT :</strong>{{driverInfo.soat}}<br />\n          <strong>PLACA :</strong>{{driverInfo.license_plate}}<br />\n          <strong>Licensia :</strong>{{driverInfo.type_license}}<br />\n        </ion-card-content>\n      </ion-card>\n    </div>\n    <div class=\"info_driver\">\n      <ion-card>\n        <ion-card-header>\n          <ion-card-title>I<strong>nformación Viaje</strong></ion-card-title>\n        </ion-card-header>\n\n        <ion-card-content>\n          <div *ngIf=\"serviceInfo.tipoVehiculo === 1\">\n            <strong><h1>Carry</h1></strong>\n            <img src=\"assets/img/carry.jpeg\" width=\"330px\" />\n          </div>\n          <div *ngIf=\"serviceInfo.tipoVehiculo === 2\">\n            <strong><h1>TIPO LUV</h1></strong>\n            <img src=\"assets/img/luv.jpeg\" width=\"330px\" />\n          </div>\n          <div *ngIf=\"serviceInfo.tipoVehiculo === 3\">\n            <strong> <h1>TIPO 150</h1></strong>\n            <img src=\"assets/img/150.jpeg\" width=\"330px\" />\n          </div>\n          <div *ngIf=\"serviceInfo.tipoVehiculo === 4\">\n            <strong> <h1>TIPO 250</h1></strong>\n            <img src=\"assets/img/250.jpeg\" width=\"330px\" />\n          </div>\n          <div *ngIf=\"serviceInfo.tipoVehiculo === 5\">\n            <strong><h1>TIPO 450</h1></strong>\n            <img src=\"assets/img/450.jpeg\" width=\"330px\" />\n          </div>\n          <div *ngIf=\"serviceInfo.tipoVehiculo === 6\">\n            <strong><h1>TIPO 600</h1></strong>\n            <img src=\"assets/img/600.jpeg\" width=\"330px\" />\n          </div>\n          <strong>Fecha creación : :</strong>{{serviceInfo.date | date}}<br />\n          <strong>Fecha recogida :</strong>{{serviceInfo.fechaRecogida}}<br />\n          <strong>Tipo de vehículo :</strong>{{serviceInfo.tipoVehiculo}}<br />\n          <strong>Anotaciones :</strong>{{serviceInfo.anotaciones}}<br />\n        </ion-card-content>\n      </ion-card>\n    </div>\n    <ion-button\n      class=\"goodfont boton\"\n      margin-bottom\n      mode=\"ios\"\n      *ngIf=\"serviceInfo.status === 'TAKEN' && driverInfo && driverInfo.type === 'operator' \"\n      expand=\"block\"\n      (click)=\"terminarViaje()\"\n      >Terminar</ion-button\n    >\n    <br />\n    <br />\n    <br />\n    <br />\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/service-detail/service-detail.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/service-detail/service-detail.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p {\n  margin-top: 0px;\n  margin-bottom: 5px;\n  color: rgba(128, 128, 128, 0.8); }\n\n.item-profile {\n  width: 100%;\n  background-position: center center;\n  background-size: cover; }\n\n.item-detail {\n  width: 100%;\n  background: white;\n  position: absolute; }\n\n.payment {\n  height: 50px;\n  display: flex;\n  margin: auto;\n  margin-top: 20px;\n  margin-bottom: 20px; }\n\n.driver-card {\n  border-top: 1px solid rgba(128, 128, 128, 0.3);\n  border-bottom: 1px solid rgba(128, 128, 128, 0.3);\n  display: flex;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  align-items: center;\n  justify-content: space-between; }\n\n.driver-card img {\n    height: 50px; }\n\n.driver-card .detail-user {\n    flex: 2;\n    padding-left: 10px; }\n\n.driver-card .detail-user p {\n      text-align: left;\n      margin: 0; }\n\n.driver-card .blue {\n    color: #4c6ade;\n    font-weight: bold; }\n\n.address-container {\n  display: flex;\n  justify-content: space-around; }\n\n.address-container div p strong {\n    color: black; }\n\niframe {\n  width: 100%;\n  height: 200px;\n  border: none; }\n\n.info_driver {\n  font-size: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VydmljZS1kZXRhaWwvQzpcXFVzZXJzXFxBbGV4YW5kZXJWYXJnYXNcXERvY3VtZW50c1xcZGV2ZWxvcG1lbnRzeXNcXGFjYXJyZW9zL3NyY1xcYXBwXFxzZXJ2aWNlLWRldGFpbFxcc2VydmljZS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWU7RUFDZixtQkFBa0I7RUFDbEIsZ0NBQStCLEVBQ2hDOztBQUNEO0VBQ0UsWUFBVztFQUNYLG1DQUFrQztFQUNsQyx1QkFBc0IsRUFDdkI7O0FBRUQ7RUFDRSxZQUFXO0VBQ1gsa0JBQWlCO0VBQ2pCLG1CQUFrQixFQUNuQjs7QUFDRDtFQUNFLGFBQVk7RUFDWixjQUFhO0VBQ2IsYUFBWTtFQUNaLGlCQUFnQjtFQUNoQixvQkFBbUIsRUFDcEI7O0FBQ0Q7RUFDRSwrQ0FBOEM7RUFDOUMsa0RBQWlEO0VBQ2pELGNBQWE7RUFDYixrQkFBaUI7RUFDakIscUJBQW9CO0VBQ3BCLG9CQUFtQjtFQUNuQiwrQkFBOEIsRUFnQi9COztBQXZCRDtJQVNJLGFBQVksRUFDYjs7QUFWSDtJQVlJLFFBQU87SUFDUCxtQkFBa0IsRUFLbkI7O0FBbEJIO01BZU0saUJBQWdCO01BQ2hCLFVBQVMsRUFDVjs7QUFqQkw7SUFvQkksZUFBYztJQUNkLGtCQUFpQixFQUNsQjs7QUFHSDtFQUNFLGNBQWE7RUFDYiw4QkFBNkIsRUFROUI7O0FBVkQ7SUFNUSxhQUFZLEVBQ2I7O0FBS1A7RUFDRSxZQUFXO0VBQ1gsY0FBYTtFQUNiLGFBQVksRUFDYjs7QUFFRDtFQUNFLGdCQUFlLEVBQ2hCIiwiZmlsZSI6InNyYy9hcHAvc2VydmljZS1kZXRhaWwvc2VydmljZS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XHJcbiAgbWFyZ2luLXRvcDogMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxuICBjb2xvcjogcmdiYSgxMjgsIDEyOCwgMTI4LCAwLjgpO1xyXG59XHJcbi5pdGVtLXByb2ZpbGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLml0ZW0tZGV0YWlsIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuLnBheW1lbnQge1xyXG4gIGhlaWdodDogNTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuLmRyaXZlci1jYXJkIHtcclxuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgxMjgsIDEyOCwgMTI4LCAwLjMpO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDEyOCwgMTI4LCAxMjgsIDAuMyk7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBpbWcge1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gIH1cclxuICAuZGV0YWlsLXVzZXIge1xyXG4gICAgZmxleDogMjtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgIHAge1xyXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5ibHVlIHtcclxuICAgIGNvbG9yOiAjNGM2YWRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgfVxyXG59XHJcblxyXG4uYWRkcmVzcy1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgZGl2IHtcclxuICAgIHAge1xyXG4gICAgICBzdHJvbmcge1xyXG4gICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuaWZyYW1lIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIGJvcmRlcjogbm9uZTtcclxufVxyXG5cclxuLmluZm9fZHJpdmVyIHtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/service-detail/service-detail.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/service-detail/service-detail.page.ts ***!
  \*******************************************************/
/*! exports provided: ServiceDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceDetailPage", function() { return ServiceDetailPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/services.service */ "./src/app/services/services.service.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var ServiceDetailPage = /** @class */ (function () {
    function ServiceDetailPage(activatedRoute, servicesService, loadingController, alertController, rout) {
        this.activatedRoute = activatedRoute;
        this.servicesService = servicesService;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.rout = rout;
        this.myServiceId = null;
        this.serviceInfo = null;
        this.driverInfo = {
            name: "",
            phone: "",
            tecno: "",
            soat: "",
            license_plate: "",
            type_license: "",
        };
        this.firstTime = 0;
    }
    ServiceDetailPage.prototype.encodeURL = function (url) {
        return encodeURIComponent(url);
    };
    ServiceDetailPage.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.myServiceId = _this.activatedRoute.snapshot.paramMap.get("myServiceId");
            _this.servicesService
                .acarreoDetail(_this.myServiceId)
                .then(function (doc) {
                _this.serviceInfo = doc;
                _this.getInfoConductor(_this.serviceInfo);
                _this.buscarConductor(_this.serviceInfo);
            })
                .catch(function (err) {
                console.log(err);
                alert("Este viaje no existe");
            });
        }, 2000);
    };
    ServiceDetailPage.prototype.buscarConductor = function (doc) {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(doc.driverId === 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.alertController.create({
                                cssClass: "my-custom-class",
                                header: "Buscando conductor ...",
                                buttons: [
                                    {
                                        text: "Cancelar",
                                        handler: function () {
                                            console.warn({
                                                SERVICO: _this.serviceInfo,
                                                ssID: _this.myServiceId,
                                            });
                                            _this.servicesService.deleteTrip(_this.myServiceId).then(function (res) {
                                                console.log(res);
                                                _this.rout.navigateByUrl("/tabs/service");
                                                window.alert("Viaje cancelado");
                                            });
                                            console.log("Confirm Okay");
                                        },
                                    },
                                ],
                            })];
                    case 1:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 2:
                        _a.sent();
                        /*
                        const loading = await this.loadingController.create({
                          cssClass: "my-custom-class",
                          message: "Buscando conductor ...",
                        });
                        await loading.present();*/
                        this.servicesService
                            .suscribeAcarreo(this.myServiceId)
                            .subscribe(function (cngs) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        this.serviceInfo = doc;
                                        setTimeout(function () {
                                            _this.getInfoConductor(_this.serviceInfo);
                                        }, 2000);
                                        this.firstTime++;
                                        if (!(this.firstTime > 1)) return [3 /*break*/, 2];
                                        setTimeout(function () {
                                            _this.getInfoConductor(_this.serviceInfo);
                                        }, 2000);
                                        return [4 /*yield*/, alert_1.dismiss()];
                                    case 1:
                                        _a.sent();
                                        _a.label = 2;
                                    case 2: return [2 /*return*/];
                                }
                            });
                        }); });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ServiceDetailPage.prototype.getInfoConductor = function (serviceInfo) {
        var _this = this;
        console.warn(serviceInfo);
        if (serviceInfo.driverId.length > 2) {
            this.servicesService
                .getProfile(serviceInfo.driverId)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1))
                .subscribe(function (driver) {
                _this.driverInfo = driver[0].payload.doc.data();
                console.warn(_this.driverInfo);
            });
        }
    };
    ServiceDetailPage.prototype.terminarViaje = function () {
        this.presentAlertConfirm();
    };
    ServiceDetailPage.prototype.presentAlertConfirm = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            cssClass: "my-custom-class",
                            header: "Terminar Viaje",
                            message: "¿Deseas terminar el viaje?",
                            buttons: [
                                {
                                    text: "Cancelar",
                                    role: "cancel",
                                    cssClass: "secondary",
                                    handler: function (blah) {
                                        console.log("Cancelar Okay");
                                    },
                                },
                                {
                                    text: "Aceptar",
                                    handler: function () {
                                        _this.servicesService
                                            .updateTrip(__assign({}, _this.serviceInfo, { status: "CLOSED", driverId: _this.serviceInfo.driverId || "" }))
                                            .then(function (res) {
                                            console.log(res);
                                            _this.rout.navigateByUrl("/trips");
                                            window.alert("Puedes consultar todos tus viajes aquí");
                                        });
                                        console.log("Confirm Okay");
                                    },
                                },
                            ],
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ServiceDetailPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-service-detail",
            template: __webpack_require__(/*! ./service-detail.page.html */ "./src/app/service-detail/service-detail.page.html"),
            styles: [__webpack_require__(/*! ./service-detail.page.scss */ "./src/app/service-detail/service-detail.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ServiceDetailPage);
    return ServiceDetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=10.js.map