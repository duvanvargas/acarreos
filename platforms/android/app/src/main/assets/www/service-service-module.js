(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["service-service-module"],{

/***/ "./src/app/service/service.module.ts":
/*!*******************************************!*\
  !*** ./src/app/service/service.module.ts ***!
  \*******************************************/
/*! exports provided: ServicePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicePageModule", function() { return ServicePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _service_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./service.page */ "./src/app/service/service.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _service_page__WEBPACK_IMPORTED_MODULE_5__["ServicePage"]
    }
];
var ServicePageModule = /** @class */ (function () {
    function ServicePageModule() {
    }
    ServicePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_service_page__WEBPACK_IMPORTED_MODULE_5__["ServicePage"]]
        })
    ], ServicePageModule);
    return ServicePageModule;
}());



/***/ }),

/***/ "./src/app/service/service.page.html":
/*!*******************************************!*\
  !*** ./src/app/service/service.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title text-center>Servicios</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div>\n    <img src=\"assets/img/logoicon.png\" class=\"login-image\" />\n    <p text-center>Selecciona tus servicio</p>\n    <div class=\"services-container\">\n      <div class=\"service-card\" (click)=\"openItem(1)\">\n        <img src=\"assets/img/acarreos.PNG\" alt=\"\" />\n        <div>\n          <strong>ACARREOS</strong>\n          <small\n            >Transporte de carga de menor tamaño, a nivel urbano y nacional.\n          </small>\n        </div>\n      </div>\n      <div class=\"service-card\" (click)=\"openItem(2)\">\n        <img src=\"assets/img/mudanza.PNG\" alt=\"\" />\n        <div>\n          <strong>MUDANZA</strong>\n          <small\n            >Servicio de transporte de carga de gran tamaño, a nivel urbano y\n            nacional.\n          </small>\n        </div>\n      </div>\n      <div class=\"service-card\" (click)=\"openItem(3)\">\n        <img src=\"assets/img/grua.PNG\" alt=\"\" />\n        <div>\n          <strong>GRÚA CARRO</strong>\n          <small\n            >Servicio de transporte de motos, en grúa o camioneta, con vehículos\n            debidamente equipados.\n          </small>\n        </div>\n      </div>\n      <div class=\"service-card\" (click)=\"openItem(4)\">\n        <img src=\"assets/img/moto.PNG\" alt=\"\" />\n        <div>\n          <strong>GRÚA MOTO</strong>\n          <small>Servicios de grúa de manera inmediata. </small>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/service/service.page.scss":
/*!*******************************************!*\
  !*** ./src/app/service/service.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-container {\n  display: flex; }\n  .button-container button {\n    margin: 3px; }\n  .services-container .service-card {\n  border: 1px solid rgba(128, 128, 128, 0.49);\n  border-radius: 5px;\n  margin: 5px;\n  padding: 5px;\n  display: flex;\n  text-align: center;\n  background: white; }\n  .services-container .service-card.disabled {\n    opacity: 0.5;\n    pointer-events: none; }\n  .services-container .service-card img {\n    height: 80px; }\n  .services-container .service-card div {\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    font-size: 20px;\n    padding-left: 20px;\n    margin: auto; }\n  .services-container .service-card div small {\n      color: rgba(88, 88, 88, 0.6); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VydmljZS9DOlxcVXNlcnNcXEFsZXhhbmRlclZhcmdhc1xcRG9jdW1lbnRzXFxkZXZlbG9wbWVudHN5c1xcYWNhcnJlb3Mvc3JjXFxhcHBcXHNlcnZpY2VcXHNlcnZpY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYSxFQUlkO0VBTEQ7SUFHSSxZQUFXLEVBQ1o7RUFFSDtFQUVJLDRDQUEyQztFQUMzQyxtQkFBa0I7RUFDbEIsWUFBVztFQUNYLGFBQVk7RUFDWixjQUFhO0VBQ2IsbUJBQWtCO0VBQ2xCLGtCQUFpQixFQW9CbEI7RUE1Qkg7SUFVTSxhQUFZO0lBQ1oscUJBQW9CLEVBQ3JCO0VBWkw7SUFjTSxhQUFZLEVBQ2I7RUFmTDtJQWlCTSxjQUFhO0lBQ2IsdUJBQXNCO0lBQ3RCLHdCQUF1QjtJQUN2QixvQkFBbUI7SUFDbkIsZ0JBQWU7SUFDZixtQkFBa0I7SUFDbEIsYUFBWSxFQUliO0VBM0JMO01BeUJRLDZCQUE0QixFQUM3QiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2Uvc2VydmljZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnV0dG9uLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBidXR0b24ge1xyXG4gICAgbWFyZ2luOiAzcHg7XHJcbiAgfVxyXG59XHJcbi5zZXJ2aWNlcy1jb250YWluZXIge1xyXG4gIC5zZXJ2aWNlLWNhcmQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgxMjgsIDEyOCwgMTI4LCAwLjQ5KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIG1hcmdpbjogNXB4O1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgJi5kaXNhYmxlZCB7XHJcbiAgICAgIG9wYWNpdHk6IDAuNTtcclxuICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgICB9XHJcbiAgICBpbWcge1xyXG4gICAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICB9XHJcbiAgICBkaXYge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgc21hbGwge1xyXG4gICAgICAgIGNvbG9yOiByZ2JhKDg4LCA4OCwgODgsIDAuNik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/service/service.page.ts":
/*!*****************************************!*\
  !*** ./src/app/service/service.page.ts ***!
  \*****************************************/
/*! exports provided: ServicePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicePage", function() { return ServicePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ServicePage = /** @class */ (function () {
    function ServicePage(rout) {
        this.rout = rout;
    }
    ServicePage.prototype.ngOnInit = function () { };
    /**
     * Navigate to the detail page for this item.
     */
    ServicePage.prototype.openItem = function (index) {
        switch (index) {
            case 1:
                this.rout.navigateByUrl("/tabs/service/formulario");
                break;
            case 2:
                this.rout.navigateByUrl("/tabs/service/formularioMudanza");
                break;
            case 3:
                this.rout.navigateByUrl("/tabs/service/formularioGrua?type=carro");
                break;
            case 4:
                this.rout.navigateByUrl("/tabs/service/formularioGrua?type=moto");
                break;
            default:
                break;
        }
    };
    ServicePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-service",
            template: __webpack_require__(/*! ./service.page.html */ "./src/app/service/service.page.html"),
            styles: [__webpack_require__(/*! ./service.page.scss */ "./src/app/service/service.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ServicePage);
    return ServicePage;
}());



/***/ })

}]);
//# sourceMappingURL=service-service-module.js.map