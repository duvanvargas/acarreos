import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { RegisterOperatorPage } from "./register-operator.page";

const routes: Routes = [
  {
    path: "",
    component: RegisterOperatorPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [RegisterOperatorPage],
})
export class RegisterOperatorPageModule {}
