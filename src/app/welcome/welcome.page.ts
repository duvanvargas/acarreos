import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-welcome",
  templateUrl: "./welcome.page.html",
  styleUrls: ["./welcome.page.scss"],
})
export class WelcomePage implements OnInit {
  selectedLogin = 0;
  constructor(private rout: Router) {}

  ngOnInit() {}

  selectLogin(typeLogin) {
    this.selectedLogin = typeLogin;
  }

  login() {
    this.rout.navigateByUrl("/login");
  }

  signup() {
    if (this.selectedLogin === 1) {
      this.rout.navigateByUrl("/register");
    } else {
      this.rout.navigateByUrl("/registerOperator");
    }
  }
}
