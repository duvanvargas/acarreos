import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", loadChildren: "./main/main.module#MainPageModule" },
  { path: "login", loadChildren: "./login/login.module#LoginPageModule" },
  {
    path: "register",
    loadChildren: "./register/register.module#RegisterPageModule",
  },
  {
    path: "registerOperator",
    loadChildren:
      "./register-operator/register-operator.module#RegisterOperatorPageModule",
  },
  {
    path: "edit-profile",
    loadChildren: "./edit-profile/edit-profile.module#EditProfilePageModule",
  },
  { path: "main", loadChildren: "./main/main.module#MainPageModule" },
  {
    path: "welcome",
    loadChildren: "./welcome/welcome.module#WelcomePageModule",
  },
  { path: "intro", loadChildren: "./intro/intro.module#IntroPageModule" },
  { path: "tabs", loadChildren: "./tabs/tabs.module#TabsPageModule" },
  { path: "stats", loadChildren: "./stats/stats.module#StatsPageModule" },
  {
    path: "service",
    loadChildren: "./service/service.module#ServicePageModule",
  },
  {
    path: "service-detail",
    loadChildren:
      "./service-detail/service-detail.module#ServiceDetailPageModule",
  },
  { path: "trips", loadChildren: "./trips/trips.module#TripsPageModule" },
  {
    path: "settings",
    loadChildren: "./settings/settings.module#SettingsPageModule",
  },
  { path: "logout", loadChildren: "./logout/logout.module#LogoutPageModule" },  { path: 'recover', loadChildren: './recover/recover.module#RecoverPageModule' },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
