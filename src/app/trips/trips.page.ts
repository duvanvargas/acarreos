import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { take } from "rxjs/operators";
import { ServicesService } from "../services/services.service";

@Component({
  selector: "app-trips",
  templateUrl: "./trips.page.html",
  styleUrls: ["./trips.page.scss"],
})
export class TripsPage implements OnInit {
  tripsHistory = [];
  constructor(
    private servicesService: ServicesService,
    private aut: AngularFireAuth,
    public rout: Router,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.aut.authState.pipe(take(1)).subscribe((user) => {
      if (user) {
        this.servicesService.getAllAcarreos(user.uid).then((res) => {
          res.forEach((element) => {
            const data = element.data();
            console.log(data);
            this.tripsHistory.push({
              id: element.id,
              data: data,
            });
          });
        });
      }
    });
  }

  detailTrip(id) {
    this.rout.navigateByUrl("/tabs/service/" + id);
  }

  translateStatus(status) {
    if (status === "TAKEN") {
      return "En curso";
    }
    if (status === "PENDING") {
      return "Pendiente";
    }
    if (status === "CLOSED") {
      return "Finalizado";
    }
    return status;
  }
}
