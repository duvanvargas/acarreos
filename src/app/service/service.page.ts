import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-service",
  templateUrl: "./service.page.html",
  styleUrls: ["./service.page.scss"],
})
export class ServicePage implements OnInit {
  constructor(private rout: Router) {}

  ngOnInit() {}

  /**
   * Navigate to the detail page for this item.
   */
  openItem(index) {
    switch (index) {
      case 1:
        this.rout.navigateByUrl("/tabs/service/formulario");
        break;
      case 2:
        this.rout.navigateByUrl("/tabs/service/formularioMudanza");
        break;
      case 3:
        this.rout.navigateByUrl("/tabs/service/formularioGrua?type=carro");
        break;
      case 4:
        this.rout.navigateByUrl("/tabs/service/formularioGrua?type=moto");
        break;

      default:
        break;
    }
  }
}
