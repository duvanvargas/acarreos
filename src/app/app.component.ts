import { Component } from "@angular/core";

import { AlertController, Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase/app";
import { Router } from "@angular/router";
import { ThemeService } from "./services/theme.service";
import { OneSignal } from "@ionic-native/onesignal/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public aut: AngularFireAuth,
    private rout: Router,
    private theme: ThemeService,
    private oneSignal: OneSignal,
    private alertCtrl: AlertController
  ) {
    this.initializeApp();
    if (localStorage.getItem("theme") === "dark") {
      this.theme.enableDark();
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.platform.is("cordova")) {
        this.setupPush();
      } else {
        console.warn("SIN ONESIGNAL - BROWSER");
      }
    });
    this.aut.authState.subscribe(
      (user) => {
        if (user) {
          // this.rout.navigateByUrl('');
        } else {
          console.log("Primer redirect");
          this.rout.navigateByUrl("/welcome");
        }
      },
      () => {
        // this.rout.navigateByUrl('/login');
      }
    );
  }

  setupPush() {
    console.warn("Iniciar ONESIGNAL");
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit(
      "a8ef05a8-430d-44f6-a70f-cd2519e12578",
      "39790583737"
    );

    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.None
    );

    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe((data) => {
      /*let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);*/
    });

    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe((data) => {
      // Just a note that the data is a different place here!
      let additionalData = data.notification.payload.additionalData;

      /*this.showAlert(
        "Notification opened",
        "You already read this before",
        additionalData.task
      );*/
    });

    this.oneSignal.endInit();
    this.oneSignal.getIds().then((identity) => {
      //alert(identity.pushToken + " It's Push Token");
      //alert(identity.userId + " It's Devices ID");
      localStorage.setItem("userId", identity.userId);
    });
  }
  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          },
        },
      ],
    });
    alert.present();
  }
}
