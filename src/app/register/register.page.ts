import { Component } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase/app";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ServicesService } from "../services/services.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"],
})
export class RegisterPage {
  email: string = "";
  password: string = "";
  cpassword: string = "";
  name: string = "";
  address: string = "";
  phone: string = "";

  passwordType = "password";
  passwordIcon = "eye-off";

  constructor(
    public afr: AngularFireAuth,
    public rout: Router,
    private services: ServicesService,
    public alertController: AlertController
  ) {}

  async register() {
    const { email, password, cpassword } = this;

    if (password !== cpassword) {
      this.errorpassIguales();
      this.rout.navigate(["/register"]);
    } else {
      try {
        await this.afr.auth
          .createUserWithEmailAndPassword(email, password)
          .then((data) => {
            //Registar información personal
            const dataUser = {
              name: this.name,
              phone: this.phone,
              mail: this.email,
              adress: this.address,
              uid: data.user.uid,
              username: "null",
              tokenPush: localStorage.getItem("userId") || "-",
              img: "null",
            };
            this.services.createUser(dataUser).then((res) => {
              this.rout.navigateByUrl("");
            });
          });
      } catch (error) {
        console.log(error);
        if (error.code === "auth/wrong-password") {
          this.error("Contraseña invalida");
        }
        if (error.code === "auth/user-not-found") {
          this.error("Usuario no encontrado");
        }
        if (error.code === "auth/email-already-in-use") {
          this.error("Usuario ya existente");
        }
        if (error.code === "auth/argument-error") {
          this.error("Argument error");
        }
        if (error.code === "auth/invalid-email") {
          this.error("Correo invalido");
        }
      }
    }
  }
  goLogin() {
    this.rout.navigate(["/login"]);
  }

  async errorpassIguales() {
    const alert = await this.alertController.create({
      message: "Las contraseñas no son iguales",
      buttons: ["OK"],
    });

    await alert.present();
  }

  async errorServ() {
    const alert = await this.alertController.create({
      message: "Algo ocurrió , intenta mas tarde",
      buttons: ["OK"],
    });

    await alert.present();
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  async error(mensaje: string) {
    const alert = await this.alertController.create({
      message: mensaje,
      buttons: ["OK"],
    });
    await alert.present();
  }
  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }
}
