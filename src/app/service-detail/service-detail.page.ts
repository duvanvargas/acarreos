import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController, LoadingController } from "@ionic/angular";
import { take } from "rxjs/operators";
import { ServicesService } from "../services/services.service";
@Component({
  selector: "app-service-detail",
  templateUrl: "./service-detail.page.html",
  styleUrls: ["./service-detail.page.scss"],
})
export class ServiceDetailPage implements OnInit {
  myServiceId = null;
  serviceInfo = null;
  driverInfo = {
    name: "",
    phone: "",
    tecno: "",
    soat: "",
    license_plate: "",
    type_license: "",
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesService: ServicesService,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public rout: Router
  ) {}

  encodeURL(url) {
    return encodeURIComponent(url);
  }

  ngOnInit() {
    setTimeout(() => {
      this.myServiceId = this.activatedRoute.snapshot.paramMap.get(
        "myServiceId"
      );
      this.servicesService
        .acarreoDetail(this.myServiceId)
        .then((doc) => {
          this.serviceInfo = doc;
          this.getInfoConductor(this.serviceInfo);
          this.buscarConductor(this.serviceInfo);
        })
        .catch((err) => {
          console.log(err);
          alert("Este viaje no existe");
        });
    }, 2000);
  }

  firstTime = 0;
  async buscarConductor(doc) {
    if (doc.driverId === 0) {
      const alert = await this.alertController.create({
        cssClass: "my-custom-class",
        header: "Buscando conductor ...",
        buttons: [
          {
            text: "Cancelar",
            handler: () => {
              console.warn({
                SERVICO: this.serviceInfo,
                ssID: this.myServiceId,
              });
              this.servicesService.deleteTrip(this.myServiceId).then((res) => {
                console.log(res);
                this.rout.navigateByUrl("/tabs/service");
                window.alert("Viaje cancelado");
              });
              console.log("Confirm Okay");
            },
          },
        ],
      });

      await alert.present();

      /*
      const loading = await this.loadingController.create({
        cssClass: "my-custom-class",
        message: "Buscando conductor ...",
      });
      await loading.present();*/
      this.servicesService
        .suscribeAcarreo(this.myServiceId)
        .subscribe(async (cngs) => {
          this.serviceInfo = doc;
          setTimeout(() => {
            this.getInfoConductor(this.serviceInfo);
          }, 2000);
          this.firstTime++;
          if (this.firstTime > 1) {
            setTimeout(() => {
              this.getInfoConductor(this.serviceInfo);
            }, 2000);
            await alert.dismiss();
          }
        });
    }
  }

  getInfoConductor(serviceInfo) {
    console.warn(serviceInfo);
    if (serviceInfo.driverId.length > 2) {
      this.servicesService
        .getProfile(serviceInfo.driverId)
        .pipe(take(1))
        .subscribe((driver) => {
          this.driverInfo = driver[0].payload.doc.data();
          console.warn(this.driverInfo);
        });
    }
  }

  terminarViaje() {
    this.presentAlertConfirm();
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Terminar Viaje",
      message: "¿Deseas terminar el viaje?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Cancelar Okay");
          },
        },
        {
          text: "Aceptar",
          handler: () => {
            this.servicesService
              .updateTrip({
                ...this.serviceInfo,
                status: "CLOSED",
                driverId: this.serviceInfo.driverId || "",
              })
              .then((res) => {
                console.log(res);
                this.rout.navigateByUrl("/trips");
                window.alert("Puedes consultar todos tus viajes aquí");
              });
            console.log("Confirm Okay");
          },
        },
      ],
    });

    await alert.present();
  }
}
