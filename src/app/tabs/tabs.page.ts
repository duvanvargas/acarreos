import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { ServicesService } from "../services/services.service";
import { AlertController } from "@ionic/angular";
import { take } from "rxjs/operators";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.page.html",
  styleUrls: ["./tabs.page.scss"],
})
export class TabsPage implements OnInit {
  firstload = true;
  userDATA;
  constructor(
    private servicesService: ServicesService,
    private aut: AngularFireAuth,
    public rout: Router,
    public alertController: AlertController,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.aut.authState.pipe(take(1)).subscribe((user) => {
      if (user) {
        this.getProfile(user.uid);
      }
    });
  }

  async getProfile(id) {
    await this.servicesService
      .getProfile(id)
      .pipe(take(1))
      .subscribe((data) => {
        this.userDATA = data[0].payload.doc.data();
        if (this.userDATA.type === "operator") {
          this.servicesService.suscribeLastAcarreos(id).subscribe((res) => {
            res.forEach((element) => {
              const data = element.payload.doc.data();
              data.id = element.payload.doc.id;
              if (
                data.driverId === 0 &&
                parseInt(this.userDATA.type_car) === parseInt(data.tipoVehiculo)
              ) {
                this.firstload = false;
                this.presentAlertConfirm(data);
              }
            });
          });
        }
        //type_car para despues filtrar por servicios de mi categoria
      });
  }

  async presentAlertConfirm(data) {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Viaje nuevo",
      message: "¿Deseas aceptar un nuevo viaje?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Cancelar Okay");
          },
        },
        {
          text: "Aceptar",
          handler: () => {
            this.servicesService
              .updateTrip({
                ...data,
                status: "TAKEN",
                driverId: this.userDATA.uid,
              })
              .then((res) => {
                console.log(res);
                this.rout.navigateByUrl("/tabs/trips");
                window.alert("Puedes consultar todos tus viajes aquí");
              });
            console.log("Confirm Okay");
          },
        },
      ],
    });

    await alert.present();
  }
}
