import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "",
    component: TabsPage,
    children: [
      {
        path: "profile",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../edit-profile/edit-profile.module").then(
                (m) => m.EditProfilePageModule
              ),
          },
        ],
      },
      {
        path: "stats",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../stats/stats.module").then((m) => m.StatsPageModule),
          },
        ],
      },
      {
        path: "trips",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../trips/trips.module").then((m) => m.TripsPageModule),
          },
        ],
      },
      {
        path: "logout",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../logout/logout.module").then((m) => m.LogoutPageModule),
          },
        ],
      },
      {
        path: "settings",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../settings/settings.module").then(
                (m) => m.SettingsPageModule
              ),
          },
        ],
      },
      {
        path: "service",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../service/service.module").then(
                (m) => m.ServicePageModule
              ),
          },
          {
            path: "formulario",
            loadChildren: () =>
              import("../formulario/formulario.module").then(
                (m) => m.FormularioPageModule
              ),
          },
          {
            path: "formularioMudanza",
            loadChildren: () =>
              import("../formularioMudanza/formularioMudanza.module").then(
                (m) => m.FormularioMudanzaPageModule
              ),
          },
          {
            path: "formularioMudanza",
            loadChildren: () =>
              import("../formularioMudanza/formularioMudanza.module").then(
                (m) => m.FormularioMudanzaPageModule
              ),
          },
          {
            path: "formularioGrua",
            loadChildren: () =>
              import("../formularioGrua/formularioGrua.module").then(
                (m) => m.FormularioGruaPageModule
              ),
          },
          {
            path: ":myServiceId",
            loadChildren: () =>
              import("../service-detail/service-detail.module").then(
                (m) => m.ServiceDetailPageModule
              ),
          },
        ],
      },
      {
        path: "",
        redirectTo: "/tabs/profile",
        pathMatch: "full",
      },
    ],
  },
  {
    path: "",
    redirectTo: "/tabs/profile",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [TabsPage],
})
export class TabsPageModule {}
