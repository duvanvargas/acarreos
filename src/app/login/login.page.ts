import { Component } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import * as firebase from "firebase/app";
import { take } from "rxjs/operators";
import { ServicesService } from "../services/services.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage {
  username: string;
  password: string;

  passwordType = "password";
  passwordIcon = "eye-off";

  constructor(
    public afs: AngularFireAuth,
    public rout: Router,
    public alertController: AlertController,
    private services: ServicesService
  ) {}

  async login() {
    const { username, password } = this;
    console.log(username, password);
    try {
      const res = await this.afs.auth.signInWithEmailAndPassword(
        username,
        password
      );
      console.log(res);

      await this.services
        .getProfile(res.user.uid)
        .pipe(take(1))
        .subscribe((data: any) => {
          console.log(data);
          if (data.length !== 0) {
            let tempData = data[0].payload.doc.data();
            this.username = data[0].payload.doc.data().username;
            tempData.tokenPush = localStorage.getItem("userId") || "-";
            console.log("profil full", tempData);
            let id = data[0].payload.doc.id;
            this.services.updateUser(tempData, id).then((res) => {
              console.log("ACTUALIZADO" + res);
              setTimeout(() => {
                this.rout.navigateByUrl("/tabs/profile");
              }, 1000);
            });
          } else {
            console.log("profile empty");
          }
        });
    } catch (error) {
      console.log(error);
      if (error.code === "auth/wrong-password") {
        this.error("Contraseña incorrecta");
      }
      if (error.code === "auth/user-not-found") {
        this.error("Usuario no encontrado");
      }
      if (error.code === "auth/email-already-in-use") {
        this.error("Usuario ya existente");
      }
      if (error.code === "auth/argument-error") {
        this.error("Argument error");
      }
      if (error.code === "auth/invalid-email") {
        this.error("correo invalido");
      } else {
        this.error("Ocurrió un error intenta más tarde");
      }
    }
  }
  async loginGmail() {
    try {
      const res = await this.afs.auth.signInWithPopup(
        new firebase.auth.GoogleAuthProvider()
      );
      console.log(res);
      this.rout.navigateByUrl("main");
    } catch (error) {
      if (error.code === "auth/wrong-password") {
        this.error("Incorrect Password");
      }
      if (error.code === "auth/user-not-found") {
        this.error("User dont found");
      }
      if (error.code === "auth/email-already-in-use") {
        this.error("User already use");
      }
      if (error.code === "auth/argument-error") {
        this.error("Argument error");
      }
      if (error.code === "auth/invalid-email") {
        this.error("Invalid error");
      }
      console.log(error);
    }
  }

  goRegister() {
    this.rout.navigateByUrl("/welcome");
  }
  goRecoverPassword() {
    this.rout.navigateByUrl("/recover");
  }

  async error(mensaje: string) {
    const alert = await this.alertController.create({
      message: mensaje,
      buttons: ["OK"],
    });
    await alert.present();
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }
  gotoslides() {
    this.rout.navigateByUrl("/");
  }
}
