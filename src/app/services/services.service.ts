import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { Route, Router } from "@angular/router";
import { map } from "rxjs/operators";
import * as firebase from "firebase/app";
import { HTTP } from "@ionic-native/http/ngx";

@Injectable({
  providedIn: "root",
})
export class ServicesService {
  anuncios: any[] = [];
  info: any[] = [];
  private itemsCollection: AngularFirestoreCollection<any>;

  constructor(
    public afs: AngularFirestore,
    public rout: Router,
    private http: HTTP
  ) {}

  async validarDireccion(dir) {
    const direccion = encodeURIComponent(dir + ",Bogotá ,Colombia");
    return fetch(
      "https://us1.locationiq.com/v1/search.php?key=pk.46e66934c4e815acabec7b25ec109eb2&q=" +
        direccion +
        "&format=json&limit=1"
    );
  }
  goto(id) {
    this.rout.navigateByUrl(id);
  }

  // User stuff

  getProfile(id) {
    this.itemsCollection = this.afs.collection<any>(`users/${id}/profile/`);

    return this.itemsCollection.snapshotChanges().pipe(
      map((info: any[]) => {
        this.info = [];

        for (const infos of info) {
          this.info.unshift(infos);
        }

        return this.info;
      })
    );
  }

  createUser(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection(`users/${value.uid}/profile`).add({
        name: value.name,
        phone: value.phone,
        mail: value.mail,
        img: value.img,
        uid: value.uid,
        adress: value.adress,
        tokenPush: value.tokenPush,
        date: Date.now(),
        username: value.username,
        type: "client",
      });
      this.rout.navigateByUrl(`profile`);
    });
  }

  createOperator(value) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection(`users/${value.uid}/profile`).add({
        name: value.name,
        phone: value.phone,
        mail: value.mail,
        img: value.img,
        uid: value.uid,
        adress: value.adress,
        date: Date.now(),
        username: value.username,
        type: "operator",
        license_plate: value.license_plate,
        type_car: value.type_car,
        soat: value.soat,
        type_license: value.type_license,
        tecno: value.tecno,
      });
      this.rout.navigateByUrl(`profile`);
    });
  }

  createAcarreo(formData) {
    // uniq generetad id
    const id = Math.random().toString(36).substring(2);
    return new Promise<any>((resolve, reject) => {
      fetch(
        "https://7f0kxcynt6.execute-api.us-east-1.amazonaws.com/default/pthonpush"
      );
      this.afs.collection(`trips`).doc(id).set(formData);
      this.rout.navigateByUrl("/tabs/service/" + id);
    });
  }

  acarreoDetail(id) {
    let docRef = this.afs.collection("trips").doc(id);
    return new Promise<any>((resolve, reject) => {
      docRef.ref
        .get()
        .then((doc) => {
          if (doc.exists) {
            resolve(doc.data());
          } else {
            reject([]);
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  suscribeLastAcarreos(id) {
    this.itemsCollection = this.afs.collection<any>(`trips`);

    return this.itemsCollection.snapshotChanges().pipe(
      map((info: any[]) => {
        this.info = [];

        for (const infos of info) {
          this.info.unshift(infos);
        }

        return this.info;
      })
    );
  }

  suscribeAcarreo(id) {
    return this.afs
      .collection<any>(`trips`)
      .doc(id)
      .snapshotChanges()
      .pipe(
        map((action) => {
          const data = action.payload.data() as {};
          const id = action.payload.id;
          return { id, ...data };
        })
      );
  }

  getAllAcarreos(idUser) {
    let docRef = this.afs.collection("trips", (ref) =>
      ref.where("uid", "==", idUser)
    );
    return new Promise<any>((resolve, reject) => {
      docRef.ref
        .get()
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getAllStaticAcarreos(idUser) {
    let docRef = this.afs.collection("trips");
    return new Promise<any>((resolve, reject) => {
      docRef.ref
        .get()
        .then((res) => {
          console.warn(res);
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  updateUser(value, id?) {
    return this.afs
      .collection("users")
      .doc(value.uid)
      .collection("profile")
      .doc(id)
      .set(value);
  }

  updateTrip(value) {
    console.warn({ value });
    return this.afs.collection("trips").doc(value.id).ref.update(value);
  }

  deleteTrip(myServiceId) {
    return this.afs.collection("trips").doc(myServiceId).delete();
  }

  // Entry stuff

  AddEntry(description) {
    // uniq generetad id
    const id = Math.random().toString(36).substring(2);
    return new Promise<any>((resolve, reject) => {
      this.afs.collection(`entrys`).doc(id).set({
        description: description,
        id: id,
        date: Date.now(),
      });
      this.rout.navigateByUrl(`profile`);
    });
  }

  getEntrys() {
    this.itemsCollection = this.afs.collection<any>(`entrys`);

    return this.itemsCollection.snapshotChanges().pipe(
      map((info: any[]) => {
        this.info = [];

        for (const infos of info) {
          this.info.unshift(infos);
        }

        return this.info;
      })
    );
  }
}
