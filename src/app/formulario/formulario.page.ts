import { Component, OnInit, ViewChild } from "@angular/core";
import { ServicesService } from "../services/services.service";
import { AngularFireAuth } from "@angular/fire/auth";
import * as geocoder from "google-geocoder";
import { AlertController, IonSlides, LoadingController } from "@ionic/angular";
@Component({
  selector: "app-formulario",
  templateUrl: "./formulario.page.html",
  styleUrls: ["./formulario.page.scss"],
})
export class FormularioPage implements OnInit {
  /**
   * FECHA RECOGIDA
        DIMENSIONES
        UBICACIÓN
        DISTANCIA ENTRE CAMION Y LA PUERTA
        DIRECCION RECOGIDA
        DIRECCIÓN DESTINO
        CONDICIONES SERVICIO
   */
  slideOpts = {
    initialSlide: 1,
    speed: 400,
  };
  formAcarreo = {
    tipo: "Acarreo",
    date: Date.now(),
    status: "PENDING",
    userId: null,
    tipoVehiculo: 1,
    driverId: 0,
    fechaRecogida: null,
    dimensiones: {
      alto: null,
      ancho: null,
      largo: null,
    },
    dirRecogida: "",
    dirRecogidaExtra: "",
    dirDestino: "",
    dirDestinoExtra: "",
    anotaciones: "",
    distanciaCamion: "",
    objetosTrastear: "",
    cantObjetos: "",
    interior: "",
    escalera: false,
    ascensor: false,
  };
  userLogged;
  @ViewChild("slides") slides: IonSlides;

  constructor(
    private servicesService: ServicesService,
    private aut: AngularFireAuth,
    private loadingController: LoadingController,
    private alertCtrl: AlertController
  ) {}
  next() {
    this.formAcarreo.tipoVehiculo++;
  }

  prev() {
    this.formAcarreo.tipoVehiculo--;
  }
  ngOnInit() {
    this.aut.authState.subscribe((user) => {
      if (user) {
        this.userLogged = user;
      }
    });
    var geo = geocoder({
      key: "AIzaSyB1KLpJ1-CMeux7HEZZnO_CNdtfr7o0cWI",
    });
  }

  async crearViaje() {
    this.servicesService
      .validarDireccion(this.formAcarreo.dirRecogida)
      .then((resRaw) => resRaw.json())
      .then((res) => {
        console.warn(res);
        if (res.error) {
          alert("La direccion de recogida no es valida");
          return;
        }
        if (res && !res.error && res.length === 0) {
          alert("La direccion de recogida no es valida");
        } else {
          this.servicesService
            .validarDireccion(this.formAcarreo.dirDestino)
            .then((resRaw2) => resRaw2.json())
            .then((res2) => {
              console.warn(res2);
              if (res.error) {
                alert("La direccion de destino no es valida");
                return;
              }
              if (res2 && !res.error && res2.length === 0) {
                alert("La direccion de destino no es valida");
              } else {
                this.generarViaje();
              }
            });
        }
      });
  }

  async generarViaje() {
    this.formAcarreo.userId = this.userLogged.uid;
    console.log(this.formAcarreo);
    const loading = await this.loadingController.create({
      message: "Creando viaje...",
      duration: 2000,
    });
    await loading.present();
    this.servicesService.createAcarreo(this.formAcarreo);
  }

  checkForm() {
    let form = this.formAcarreo;
    return (
      this.isValid(form.fechaRecogida) &&
      this.isValid(form.dirRecogida) &&
      this.isValid(form.dirDestino)
    );
  }

  isValid(field) {
    if (field === "") return false;
    if (field === null) return false;
    return true;
  }
}
