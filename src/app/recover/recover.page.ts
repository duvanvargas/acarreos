import { Component } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import * as firebase from "firebase/app";
@Component({
  selector: "app-recover",
  templateUrl: "./recover.page.html",
  styleUrls: ["./recover.page.scss"],
})
export class RecoverPage {
  username: string = "";

  constructor(
    public afs: AngularFireAuth,
    public rout: Router,
    public alertController: AlertController
  ) {}

  recover() {
    this.afs.auth
      .sendPasswordResetEmail(this.username)
      .then(function () {
        alert(
          "Te hemos enviado un correo para que puedas reestablecer tu contraseña"
        );
      })
      .catch(function (error) {
        console.log(error);
        alert("Ocurrio un error");
      });
  }
  goLogin() {
    this.rout.navigateByUrl("/login");
  }
}
