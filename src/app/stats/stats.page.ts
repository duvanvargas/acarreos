import { Component, OnInit, ViewChild } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { Chart } from "chart.js";
import { take } from "rxjs/operators";
import { ServicesService } from "../services/services.service";

@Component({
  selector: "app-stats",
  templateUrl: "./stats.page.html",
  styleUrls: ["./stats.page.scss"],
})
export class StatsPage implements OnInit {
  constructor(
    private servicesService: ServicesService,
    private aut: AngularFireAuth,
    public rout: Router,
    private loadingController: LoadingController
  ) {}
  bars: any;
  @ViewChild("barChart") barChart;

  Confirmados = 0;
  Pendientes = 0;
  Terminados = 0;
  ngOnInit() {
    if (this.bars) {
      this.bars.reset();
    }
    this.aut.authState.pipe(take(1)).subscribe((user) => {
      if (user) {
        this.servicesService.getAllStaticAcarreos(user.uid).then((res) => {
          res.forEach((element) => {
            const data = element.data();
            console.log(data.status);
            if (data.status === "TAKEN") {
              this.Confirmados++;
            }
            if (data.status === "PENDING") {
              this.Pendientes++;
            }
            if (data.status === "CLOSED") {
              this.Terminados++;
            }
            this.renderChart();
          });
        });
      }
    });
  }

  renderChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [this.Confirmados, this.Pendientes, this.Terminados],
            backgroundColor: ["#315afe", "#8856fe", "#f7c337"],
          },
        ],
        labels: ["Confirmados", "Pendientes", "Terminados"],
      },

      options: {
        responsive: true,
        aspectRatio: 1.2,
      },
    });
  }
}
