import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { FormularioGruaPage } from "./formularioGrua.page";

const routes: Routes = [
  {
    path: "",
    component: FormularioGruaPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [FormularioGruaPage],
})
export class FormularioGruaPageModule {}
