import { Component, OnInit } from "@angular/core";
import { ServicesService } from "../services/services.service";
import { AngularFireAuth } from "@angular/fire/auth";

import { LoadingController } from "@ionic/angular";
@Component({
  selector: "app-formulario",
  templateUrl: "./formularioGrua.page.html",
  styleUrls: ["./formularioGrua.page.scss"],
})
export class FormularioGruaPage implements OnInit {
  /**
   * FECHA RECOGIDA
        DIMENSIONES
        UBICACIÓN
        DISTANCIA ENTRE CAMION Y LA PUERTA
        DIRECCION RECOGIDA
        DIRECCIÓN DESTINO
        CONDICIONES SERVICIO
   */
  formAcarreo = {
    tipo: "Grua",
    date: Date.now(),
    status: "PENDING",
    userId: null,
    driverId: 0,
    fechaRecogida: null,
    dimensiones: {
      alto: null,
      ancho: null,
      largo: null,
    },
    dirRecogida: "",
    dirRecogidaExtra: "",
    dirDestino: "",
    dirDestinoExtra: "",
    placa_vehiculo: "",
    anotaciones: "",
    distanciaCamion: "",
  };
  userLogged;
  constructor(
    private servicesService: ServicesService,
    private aut: AngularFireAuth,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.aut.authState.subscribe((user) => {
      if (user) {
        this.userLogged = user;
      }
    });
  }

  async crearViaje() {
    this.formAcarreo.userId = this.userLogged.uid;
    console.log(this.formAcarreo);
    const loading = await this.loadingController.create({
      message: "Creando viaje...",
      duration: 2000,
    });
    await loading.present();
    this.servicesService.createAcarreo(this.formAcarreo);
  }

  checkForm() {
    let form = this.formAcarreo;
    return (
      this.isValid(form.fechaRecogida) &&
      this.isValid(form.dirRecogida) &&
      this.isValid(form.placa_vehiculo)
    );
  }

  isValid(field) {
    if (field === "") return false;
    if (field === null) return false;
    return true;
  }
}
