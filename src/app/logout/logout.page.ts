import { Component, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";

@Component({
  selector: "app-logout",
  templateUrl: "./logout.page.html",
  styleUrls: ["./logout.page.scss"],
})
export class LogoutPage implements OnInit {
  constructor(private aut: AngularFireAuth, private router: Router) {}

  ngOnInit() {
    this.signOut();
  }
  async signOut() {
    const res = await this.aut.auth.signOut();
    this.router.navigateByUrl("/login");
  }
}
